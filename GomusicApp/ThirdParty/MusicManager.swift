//
//  MusicManager.swift
//  Netelly
//
//  Created by Sơn Lê on 12/15/19.
//  Copyright © 2019 Sơn Lê. All rights reserved.
//

import UIKit
import AVFoundation

enum StateAudio {
    case Idle
    case Playing
    case Pausing
    case Failed
}

extension String{
    internal func isFrameKeypath() -> Bool{
        return self.elementsEqual("frame")
    }
}

protocol NLPlayerDelegate {
    
    func nlPlayerSeekedTime(toTime:Float64, finished:Bool)
    
    func nlPlayerUpdatedTime(currentTime:Float64)

    func nlPlayerVolumeChange(oldVolume: Float, newVolume: Float)
    
    func nlPlayerBufferChange(key:String,value:Any)
    
    func nlPlayerPreparedState(_ state:StateAudio,_ message:String)

    func nlPlayerFinishing(successfully flag: Bool)
    
}

class MusicManager: NSObject {

    let TracksKey = "tracks"
    let PlayableKey = "playable"
    let DurationKey = "duration"
    var observer:Any? = nil
    var frameObserver:Any? = nil
    /* Player keys */
    let RateKeyPath = "rate"
    let StatusKeyPath = "status"
    
    var currentURL = ""
    fileprivate var playMusic: AVPlayer? = nil
    fileprivate var currentState = StateAudio.Idle
    var nlPlayerPlayDelegate:NLPlayerDelegate? = nil

    static let BufferTime = "bufferTime"
    static let StartBuffer = "StartBuffer"
    static let StopBuffer = "StopBuffer"
    let BufferingAndPlaying = "bufferingAndPlaying"
    let BufferingAndPausing = "bufferingAndPausing"
    
    var playbackBufferEmptyKeyPath:String = ""
    var playbackLikelyToKeepUpKeyPath:String = ""
    var playbackBufferFullKeyPath:String = ""
    var isAddObserVol = false
    var isRegisterStatus = false
    var currentLayer:AVPlayerLayer!
    var parentView:UIView? = nil
    
    override init() {
        super.init()
        initPlayer()
    }
    
    private static var musicManager: MusicManager = {
        let musicManger = MusicManager()
        return musicManger
    }()
    
    class func shared() ->MusicManager{
        return musicManager
    }
    
    //ACTION Music player
    //Play video return state nlPlayer
    
    fileprivate func initPlayer(){
        playMusic = AVPlayer.init()
        currentLayer = AVPlayerLayer.init(player: playMusic)
        currentLayer.videoGravity = .resizeAspectFill
        currentLayer.backgroundColor = color(0x1F2124).cgColor
        self.createAudioSession()
        playMusic?.addObserver(self, forKeyPath: RateKeyPath, options: [.new,.old], context: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(nlPlayerFinishPlaying(_:)), name: .AVPlayerItemDidPlayToEndTime, object: nil)

        self.setVolumeChangeObserver()
        let interval = CMTimeMakeWithSeconds(0.5, preferredTimescale:Int32(NSEC_PER_SEC))
        if(observer != nil){
            playMusic?.removeTimeObserver(observer!)
            observer = nil
        }
        observer = playMusic?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (time) in
            let myTime = self.getCurrentTime()
            if self.nlPlayerPlayDelegate != nil{
                self.nlPlayerPlayDelegate?.nlPlayerUpdatedTime(currentTime: myTime)
            }
        })
        playMusic?.allowsExternalPlayback = true
        playMusic?.usesExternalPlaybackWhileExternalScreenIsActive = true
    }
    
    func addPlayerToView(_ parentView:UIView){
        currentLayer.removeFromSuperlayer()
        if frameObserver != nil && self.parentView != nil{
            self.parentView!.removeObserver(self, forKeyPath: "frame")
        }
        self.parentView = parentView
        frameObserver = parentView.addObserver(self, forKeyPath: "frame", options: .new, context: nil)
        currentLayer.frame = CGRect.init(origin: .zero, size: parentView.frame.size)
        self.parentView!.layer.masksToBounds = true
        self.parentView!.layer.addSublayer(currentLayer)
        currentLayer.masksToBounds = true
        
    }
    
    fileprivate func createAudioSession(){
        do {
            /// this codes for making this app ready to takeover the device nlPlayer
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback,mode:.moviePlayback ,options: .mixWithOthers)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
             //print("error: \(error.localizedDescription)")
         }
    }
    
    func prepareToPlay(_ urlVideo: String, currentTime:Int = 0){
        resetPlayer()
        if urlVideo.count <= 0{
            if nlPlayerPlayDelegate != nil{
                nlPlayerPlayDelegate?.nlPlayerPreparedState(.Failed, "No video to play!")
            }
            return
        }
        if nlPlayerPlayDelegate != nil{
            nlPlayerPlayDelegate?.nlPlayerPreparedState(.Idle, "Preparing video...")
        }
        currentURL = urlVideo
        let urlMusic = URL(string: urlVideo)
        if urlMusic == nil{
            if nlPlayerPlayDelegate != nil{
                nlPlayerPlayDelegate?.nlPlayerPreparedState(.Failed, "No video to play!")
            }
            return
        }
        let urlAssets = AVURLAsset.init(url: urlMusic!)
        let requestedKeys = ["tracks","playable","duration"]
        urlAssets.loadValuesAsynchronously(forKeys: requestedKeys) {
            DispatchQueue.main.async {
                self.prepareToPlayAsset(urlAssets, withKeys: requestedKeys,currentTime)
            }
        }
    }
    
    fileprivate func prepareToPlayAsset(_ urlAsset:AVURLAsset, withKeys:[String],_ currentTime:Int){
        for key in withKeys {
            let error = NSErrorPointer.init(nilLiteral: ())
            let keyStatus = urlAsset.statusOfValue(forKey: key, error: error)
            if keyStatus == AVKeyValueStatus.failed{
                if error != nil{
                    if self.nlPlayerPlayDelegate != nil{
                        self.nlPlayerPlayDelegate?.nlPlayerPreparedState(.Failed, error.debugDescription)
                    }
                }
                return
            }
        }
//        NSLog("playerSource: %@",urlAsset.url.absoluteString);
            if (!urlAsset.isPlayable) {
        //        NSString * errorMsg = [NSString stringWithFormat:@"The follwoing source: %@ is not playable", asset.URL.absoluteString];
                if(self.nlPlayerPlayDelegate != nil){
                    nlPlayerPlayDelegate?.nlPlayerPreparedState(.Failed, "The item is non playable")
                }
                return;
            }
            
        let playItem = AVPlayerItem.init(asset: urlAsset, automaticallyLoadedAssetKeys: withKeys)
        playItem.addObserver(self, forKeyPath: StatusKeyPath, options: [.new, .old], context: nil)
        isRegisterStatus = true
        if (playMusic?.currentItem != playItem) {
            playMusic?.replaceCurrentItem(with: playItem)
        }
        
        if currentTime > 0 {
            MusicManager.shared().seek(Float64(currentTime))
        }
    }
    
    func resetPlayer(){
        if playMusic != nil {
            playMusic?.replaceCurrentItem(with: nil)
        }
        if currentLayer != nil {
            currentLayer.removeFromSuperlayer()
        }
        if playMusic == nil {
            initPlayer()
        }
    }
    
    func playSong(){
        if playMusic != nil {
            playMusic?.play()
        }
    }
    
    //Pause video return state nlPlayer
    func pauseSong() -> Bool{
        if playMusic != nil{
            playMusic?.pause()
            return true
        }
        return false
    }
    
    func isPlaying() -> Bool{
        return playMusic?.rate == 1
    }
    
    func releasePlayer(){
        if playMusic != nil{
            playMusic?.pause()
        }
        removeStatusObserver()
        if playMusic != nil {
            playMusic?.replaceCurrentItem(with: nil)
        }
        if currentLayer != nil {
            currentLayer.removeFromSuperlayer()
        }
        clearPlayer()
    }
    
    func setVolume(_ volume:Float) -> Float{
        return playMusic?.volume ?? 0
    }
    
    func seek(_ toTime:Float64){
        if toTime >= 0 && toTime < getDuration(){
            let timeScale = self.playMusic?.currentItem?.asset.duration.timescale ?? 0
            playMusic?.seek(to: CMTime(seconds: Double(toTime), preferredTimescale: timeScale),
                            toleranceBefore: .zero, toleranceAfter: .zero, completionHandler: { (finished) in
                if self.nlPlayerPlayDelegate != nil {
                    self.nlPlayerPlayDelegate?.nlPlayerSeekedTime(toTime: toTime, finished: finished)
                }
            })
        }
    }
    
    //FUNC for get value
    func getStateAudio() -> StateAudio{
        return currentState
    }
    
    func getVolume() -> Float{
        return playMusic?.volume ?? 0.0
    }
    
    func getVolumeDevice() -> Float{
        return AVAudioSession.sharedInstance().outputVolume
    }
    
    func getDuration() -> Float64{
        return CMTimeGetSeconds(playMusic?.currentItem?.duration ?? .zero)
    }
    
    func getCurrentTime() -> Float64{
        return CMTimeGetSeconds(playMusic?.currentItem?.currentTime() ?? .zero)
    }
    
    func setMuted(_ muted:Bool){
        playMusic?.isMuted = muted
    }
    
    func isMuted() -> Bool{
        return playMusic?.isMuted ?? false
    }
    
    func getCurrentURL()->String?{
        return currentURL
    }
    
    @objc func nlPlayerFinishPlaying(_ notification:Notification){
        if let item = notification.object as? AVPlayerItem {
            if item == playMusic?.currentItem{
                if nlPlayerPlayDelegate != nil{
                    nlPlayerPlayDelegate?.nlPlayerFinishing(successfully: true)
                }
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let oldValue = change?[.oldKey]
        let newValue = change?[.newKey]
        
        if keyPath?.isFrameKeypath() ?? false {

            if let view = (object as? UIView), view == self.parentView,self.parentView != nil {
                let rect = change?[NSKeyValueChangeKey.newKey] as! CGRect
                
                let videolayer = self.parentView!.layer.sublayers?.first
                CATransaction.begin()
                CATransaction.setAnimationDuration(0)
                CATransaction.setDisableActions(true)
                videolayer!.frame = rect
                CATransaction.commit()
                
            }
            return
        }
            
        if keyPath == "outputVolume" {
            let oldVolume = (oldValue as AnyObject).floatValue ?? 0
            let newVolume = (newValue as AnyObject).floatValue ?? 0
            if self.isMuted() {
                    if nlPlayerPlayDelegate != nil {
                        nlPlayerPlayDelegate?.nlPlayerVolumeChange(oldVolume: oldVolume, newVolume: newVolume)
                    }
                }
            }

            if keyPath == "loadedTimeRanges" {
                if let newBuffer = change?[.newKey] as? NSArray, newBuffer.count > 0 {
                    if let timerange = (newBuffer.object(at: 0) as? NSValue)?.timeRangeValue {
                        //                    let time = String.init(format: "%.5f", CMTimeGetSeconds(CMTimeAdd(timerange.start, timerange.duration)))
                        //                    print("time: \(time)")
                        let timeBuffer = CMTimeGetSeconds(timerange.duration);
                        
                        if nlPlayerPlayDelegate != nil{
                            nlPlayerPlayDelegate?.nlPlayerBufferChange(key: MusicManager.BufferTime, value: timeBuffer)
                        }
                    }
                }
            }
        
            if (object as? AVPlayerItem) == playMusic?.currentItem
                    && (keyPath == playbackBufferEmptyKeyPath
                    || keyPath == playbackLikelyToKeepUpKeyPath
                    || keyPath == playbackBufferFullKeyPath) {
                if playMusic?.currentItem?.isPlaybackBufferEmpty ?? false {
                    // rate = value of 0.0 pauses the video, while a value of 1.0 plays
                    if self.getCurrentTime() >= self.getDuration()
                        && self.getCurrentTime() > 0 {
                        //NSLog(@"END VIDEO _________");
                        if nlPlayerPlayDelegate != nil{
                            nlPlayerPlayDelegate?.nlPlayerFinishing(successfully: true)
                        }
                    }else{
                        //nlPlayer stalled
                        if nlPlayerPlayDelegate != nil{
                            nlPlayerPlayDelegate?.nlPlayerBufferChange(key: MusicManager.StartBuffer, value: 0)
                        }
                    }
                }
                if (playMusic?.currentItem?.isPlaybackLikelyToKeepUp ?? false) {
        //            [self stopBuffering];
                    if nlPlayerPlayDelegate != nil{
                        nlPlayerPlayDelegate?.nlPlayerBufferChange(key: MusicManager.StopBuffer, value: 0)
                    }
                    if(isPlaying()){
                        if nlPlayerPlayDelegate != nil {
                            nlPlayerPlayDelegate?.nlPlayerPreparedState(.Playing, "")
                        }
                    }else{
                        self.playMusic?.play()
                    }
                }
                if (self.playMusic?.currentItem?.isPlaybackBufferFull ?? false) {
//                    [self stopBuffering];
                if nlPlayerPlayDelegate != nil{
                    nlPlayerPlayDelegate?.nlPlayerBufferChange(key: MusicManager.StopBuffer, value: 0)
                }
                }
            } else if keyPath == RateKeyPath {
                if self.getCurrentTime() >= self.getDuration()
                    && self.getCurrentTime() > 0
                    && self.getDuration() > 0 {
                    if nlPlayerPlayDelegate != nil{
                        nlPlayerPlayDelegate?.nlPlayerFinishing(successfully: true)
                    }
                }else{
                    // check end video
                    if self.getCurrentTime() >= 0
                        && self.getDuration() > 0 {
                        if (playMusic?.rate == 1.0) {
                            if nlPlayerPlayDelegate != nil {
                                nlPlayerPlayDelegate?.nlPlayerPreparedState(.Playing, "")
                            }
                        } else if (playMusic?.rate == 0.0) {
                            if (playMusic?.currentItem?.status.rawValue == AVPlayer.Status.readyToPlay.rawValue) {
                                if nlPlayerPlayDelegate != nil {
                                    nlPlayerPlayDelegate?.nlPlayerPreparedState(.Pausing, "")
                                }
                            }
                        }
                    }
                }
            } else if keyPath == StatusKeyPath {
                switch (playMusic?.currentItem?.status.rawValue) {
                case AVPlayerItem.Status.failed.rawValue:
                        if nlPlayerPlayDelegate != nil{
                            nlPlayerPlayDelegate?.nlPlayerPreparedState(.Failed, "Init source failed")
                        }
                        break;
                case AVPlayerItem.Status.unknown.rawValue:
                        if nlPlayerPlayDelegate != nil {
                            nlPlayerPlayDelegate?.nlPlayerPreparedState(.Failed, "Unknown source error")
                        }
                        break;
                case AVPlayerItem.Status.readyToPlay.rawValue:
                        if (oldValue as AnyObject).intValue ?? 0 != (newValue as AnyObject).intValue ?? 0 {
                            self.playSong()
                            self.registerForPlaybackNotification()
                            
                        }
                case .none:
                    //print("Error")
                    break
                case .some(_):
                    //print("Error")
                    break
                }
            }
        }

    func setVolumeChangeObserver(){
        
        do{
            try AVAudioSession.sharedInstance().setActive(true)
            AVAudioSession.sharedInstance().addObserver(self, forKeyPath: "outputVolume", options: [.new,.old], context: nil)
            isAddObserVol = true
        }catch /*let error as NSError*/{
            //print("Erro: \(error.localizedDescription)")
        }
    }

    func removeVolumeChangeObserver(){
        if isAddObserVol{
            AVAudioSession.sharedInstance().removeObserver(self, forKeyPath: "outputVolume")
            isAddObserVol = false
        }
    }

    func removeStatusObserver() {
        if (playMusic?.currentItem != nil &&  isRegisterStatus) {
            playMusic?.currentItem?.removeObserver(self, forKeyPath:StatusKeyPath, context:nil)
            isRegisterStatus = false
        }
    }
    
    func clearPlayer(){
        if playMusic == nil {
            return
        }
        self.unregisterForPlaybackNotification()
        playMusic?.removeObserver(self, forKeyPath: RateKeyPath, context: nil)
        if observer != nil{
            playMusic?.removeTimeObserver(observer!)
        }
        if frameObserver != nil && parentView != nil {
            parentView!.removeObserver(self, forKeyPath: "frame")
        }
        NotificationCenter.default.removeObserver(self)
        frameObserver = nil
        observer = nil
        nlPlayerPlayDelegate = nil
        playMusic = nil
    }

    func registerForPlaybackNotification() {
        if playMusic?.currentItem == nil {
            return;
        }
        playbackBufferEmptyKeyPath = NSStringFromSelector(#selector(getter: playMusic?.currentItem?.isPlaybackBufferEmpty))
        playbackLikelyToKeepUpKeyPath = NSStringFromSelector(#selector(getter: playMusic?.currentItem?.isPlaybackLikelyToKeepUp))
        playbackBufferFullKeyPath = NSStringFromSelector(#selector(getter: playMusic?.currentItem?.isPlaybackBufferFull))
        
        playMusic?.currentItem?.addObserver(self, forKeyPath:playbackBufferEmptyKeyPath, options:[.new, .old], context:nil);
        playMusic?.currentItem?.addObserver(self, forKeyPath:playbackLikelyToKeepUpKeyPath, options:[.new,.old], context:nil);
        playMusic?.currentItem?.addObserver(self, forKeyPath:playbackBufferFullKeyPath, options:[.new, .old], context:nil);
        playMusic?.currentItem?.addObserver(self, forKeyPath:"loadedTimeRanges", options:[.new, .old], context:nil);
    }

    func unregisterForPlaybackNotification(){
        playMusic?.currentItem?.removeObserver(self, forKeyPath:playbackBufferEmptyKeyPath)
        playMusic?.currentItem?.removeObserver(self, forKeyPath:playbackLikelyToKeepUpKeyPath)
        playMusic?.currentItem?.removeObserver(self, forKeyPath:playbackBufferFullKeyPath)
        playMusic?.currentItem?.removeObserver(self, forKeyPath:"loadedTimeRanges")
    }
    
    deinit {
        clearPlayer()
    }
}
