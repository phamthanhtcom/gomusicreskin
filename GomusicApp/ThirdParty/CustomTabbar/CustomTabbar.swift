//
//  CustomTabbar.swift
//  teleconsultpatient
//
//  Created by Nguyen Hoa on 9/29/19.
//  Copyright © 2019 Nguyen Hoa. All rights reserved.
//

import UIKit

protocol TabSelectDelegate {
    func selectTabAt(index: Int)
}

class CustomTabbar: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var tab1: CustomTabbarItem!
    @IBOutlet weak var tab2: CustomTabbarItem!
    @IBOutlet weak var tab3: CustomTabbarItem!
    @IBOutlet weak var tab4: CustomTabbarItem!
    @IBOutlet weak var stackView: UIStackView!
    var selectIndex = 0
    
    var delegate: TabSelectDelegate?
    
    var hubCart: BadgeHub!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        addHubCartTop()
    }
    
    func addHubCartTop() {
        if hubCart == nil {
            hubCart = BadgeHub(view: tab3.centerView)
            hubCart.setCircleColor(.red, label: UIColor.white)
            hubCart.moveCircleBy(x: -8, y: 0)
            hubCart.scaleCircleSize(by: 0.7)
            hubCart.maxCount = 99
        }
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("CustomTabbar", owner: self, options: nil)
        contentView.fixInView(self)
        tab1.btnAction.tag = 0
        tab2.btnAction.tag = 1
        tab3.btnAction.tag = 2
        tab4.btnAction.tag = 3
        tab1.btnAction.addTarget(self, action: #selector(didButtonClick), for: .touchUpInside)
        tab2.btnAction.addTarget(self, action: #selector(didButtonClick), for: .touchUpInside)
        tab3.btnAction.addTarget(self, action: #selector(didButtonClick), for: .touchUpInside)
        tab4.btnAction.addTarget(self, action: #selector(didButtonClick), for: .touchUpInside)
        setSelect(atIndex: selectIndex)
    }
    
    @objc func didButtonClick(_ sender: UIButton) {
        let tag = sender.tag
        self.delegate?.selectTabAt(index: tag)
    }
    
    func hideMarketPlace(_ isHidden: Bool) {
        self.tab2.isHidden = isHidden
    }
    
    func resetTab() {
        self.tab1.showTitle(false)
        self.tab2.showTitle(false)
        self.tab3.showTitle(false)
        self.tab4.showTitle(false)
    }
    
    func setSelect(atIndex: Int) {
        selectIndex = atIndex
        resetTab()
        let arrViews = [self.tab1, self.tab2, self.tab3, self.tab4]
        for index in 0..<arrViews.count {
            let tab = arrViews[index]
            tab?.setSelect(isSelect: index == atIndex)
            tab?.showTitle(index == atIndex)
        }
    }
}

extension UIView {
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
