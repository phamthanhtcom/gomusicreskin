//
//  CustomTabbarItem.swift
//  teleconsultpatient
//
//  Created by Nguyen Hoa on 9/29/19.
//  Copyright © 2019 Nguyen Hoa. All rights reserved.
//
import Foundation
import UIKit

class CustomTabbarItem: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var imgTab: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnAction: UIButton!
    
    @IBInspectable var name: String? {
         get {
            return lbTitle.text
         }set {
            self.lbTitle.text = newValue
         }
    }
    @IBInspectable var image: UIImage = UIImage() {
        didSet{
            self.imgTab.image = image
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("CustomTabbarItem", owner: self, options: nil)
        contentView.fixInView(self)
    }
    
    func showTitle(_ isShow: Bool) {
        self.lbTitle.isHidden = !isShow
    }
    
    func setSelect(isSelect: Bool) {
        imgTab.tintColor = isSelect ? color(0x4834D4): color(0xBDBDBD)
        lbTitle.textColor = isSelect ? color(0x4834D4): color(0xBDBDBD)
    }
}
