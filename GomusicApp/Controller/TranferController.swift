//
//  TranferController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/18/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class TranferController: UIViewController {
    @IBOutlet weak var lbAdress: UILabel!
    @IBOutlet weak var statusLb: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    let mgr = SGWiFiUploadManager.shared()
    class func create() -> TranferController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! TranferController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgView.image = UIImage.init(named: "SGWiFiUpload.bundle/wifi.png")
        setupServer()
        
    }
    
    @IBAction func actionBack(_ sender: Any) {
        mgr?.stopHTTPServer()
        self.dismissDetail()
    }
    
    func loadData() {
        let server = mgr?.httpServer
        if (server?.isRunning())! {
            if HYBIPHelper.deviceIPAdress() == nil {
                self.statusLb.text = "Error, your Device is not connected to WiFi"
                return;
            }
            self.statusLb.text = "After you tap Finish, WiFi tranfer will be turned off"
            self.lbAdress.text = "http://\((mgr?.ip())!):\((mgr?.port())!)"
        } else {
            self.statusLb.text = "Error, Server Stopped"
        }
    }
    
    func setupServer(){
        let success = mgr?.startHTTPServer(atPort: 8080)
        if success! {
            mgr?.setFileUploadStartCallback({ (filename, path) in
                //NSLog("file %@ Upload start", filename!)
            })
            mgr?.setFileUploadProgressCallback({ (filename, path, progress) in
                //NSLog("File %@ on progress %f", filename!, progress)
            })
            mgr?.setFileUploadFinishCallback({ (filename, path) in
                //NSLog("File Upload Finish %@ at %@", filename!, path!)
                self.showToast(msg: "File Upload Finish")
                updateBadge()
                NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
            })
        }
        self.loadData()
    }
    
    @IBAction func actionFinish(_ sender: Any) {
        mgr?.stopHTTPServer()
        self.dismissDetail()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
         mgr?.stopHTTPServer()
    }
}
