//
//  RootLibraryController.swift
//  GomusicApp
//
//  Created by doanbk on 8/22/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class RootLibraryController: UINavigationController {

    class func create() -> RootLibraryController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! RootLibraryController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
