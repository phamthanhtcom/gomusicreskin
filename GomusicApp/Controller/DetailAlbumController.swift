//
//  DetailAlbumController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/11/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class DetailAlbumController: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var nameAlbum: UILabel!
    @IBOutlet weak var lbNodata: UILabel!
    @IBOutlet weak var btnPlayAlbum: UIButton!
    @IBOutlet weak var imgAlbum: UIImageView!
    @IBOutlet weak var numberSong: UILabel!
    @IBOutlet weak var tableView: UITableView!
    let nameSongCell = "ItemSongCell"
    let nameVideoCell = "ItemVideoCell"
    var itemAlbum : AlbumModel!
    var listSong = [MusicModel]()
    var listVideo = [VideoModel]()
    var typeListSong = true
    var indexItem = -1
    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var viewBtnPlay: UIView!
    
    var showBarMini = false {
        didSet {
            self.tableView.contentInset = showBarMini ? UIEdgeInsets.init(top: 35, left: 0, bottom: 110, right: 0) : UIEdgeInsets.init(top: 35, left: 0, bottom: 10, right: 0)
        }
    }
    
    weak var customAlert : CustomAlertDetailSong!
    class func create() -> DetailAlbumController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! DetailAlbumController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        heightHeader.constant = Device.hasTopNotch ? (44 + 250) : 250
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            showBarMini = delegate.isShowBarMini
        } else {
            showBarMini = false
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: nameSongCell, bundle: nil), forCellReuseIdentifier: nameSongCell)
        tableView.register(UINib(nibName: nameVideoCell, bundle: nil), forCellReuseIdentifier: nameVideoCell)
        viewBtnPlay.layer.cornerRadius = viewBtnPlay.frame.height / 2
        NotificationCenter.default.addObserver( self, selector: #selector(hideViewPlay), name: NSNotification.Name("HIDE_VIEW_PLAY"), object: nil )
        NotificationCenter.default.addObserver( self, selector: #selector(showViewPlay), name: NSNotification.Name("SHOW_VIEW_PLAY"), object: nil )
        loadData()
    }
    
    @objc func hideViewPlay() {
        showBarMini = false
    }
    
    @objc func showViewPlay() {
        showBarMini = true
    }
    
    func loadData() {
        nameAlbum.text = itemAlbum?.name
        numberSong.text = String.init(format: itemAlbum.isVideo ? LocalizedString("count_videos", comment: "") : LocalizedString("count_songs", comment: ""), itemAlbum?.listSong.count ?? 0)
        if typeListSong {
            listSong = getAllFileByListPath((itemAlbum?.listSong)!)
            if listSong.isEmpty {
                tableView.isHidden = true
                lbNodata.isHidden = false
            } else {
                tableView.isHidden = false
                lbNodata.isHidden = true
                tableView.reloadData()
            }
        } else {
            listVideo = getAllVideoByListPath((itemAlbum?.listSong)!)
            if listVideo.isEmpty {
                tableView.isHidden = true
                lbNodata.isHidden = false
            } else {
                tableView.isHidden = false
                lbNodata.isHidden = true
                tableView.reloadData()
            }
        }
    }
    
    @IBAction func playAlbumTapped(_ sender: Any) {
        if typeListSong {
            showBarMini = true
            playAlbumFromDetail(lstSong: self.listSong, indexPlaying: 0, isRandom: false)
        } else {
            let videoPlayer = VideoPlayerController.create()
            videoPlayer.dataVideo = self.listVideo
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                if let viewPlay = delegate.viewPlay {
                    viewPlay.pauseAndPlay()
                }
                if let tabbar = delegate.tabbar {
                    tabbar.present(videoPlayer, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        dismissDetail()
    }
    
    @IBAction func actionDetail(_ sender: Any) {
        customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertDetailSong") as? CustomAlertDetailSong
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.indexItem = indexItem
        customAlert.delegate = self
        customAlert.infoAlbum = itemAlbum
        customAlert.type = .ALBUM
        self.present(customAlert, animated: true, completion: nil)
    }
    
}
extension DetailAlbumController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if typeListSong {
            return listSong.count
        } else {
            return listVideo.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.typeListSong {
            let cell = tableView.dequeueReusableCell(withIdentifier: nameSongCell, for: indexPath) as! ItemSongCell
            cell.nameSong.text = listSong[indexPath.row].nameSong
            cell.artistName.text = listSong[indexPath.row].artist
            cell.btnShowOption.isHidden = true
            cell.index = indexPath.row
            if listSong[indexPath.row].artwork == nil {
                cell.imgSong.isHidden = true
                cell.imgHidden.isHidden = false
            } else {
                cell.imgSong.image = listSong[indexPath.row].artwork
                cell.imgHidden.isHidden = true
                cell.imgSong.isHidden = false
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: nameVideoCell, for: indexPath) as! ItemVideoCell
            let dataForCell = self.listVideo[indexPath.row]
            cell.nameVideo.text = dataForCell.nameVideo
            cell.artistName.text = dataForCell.artist
            cell.durationVideo.text = dataForCell.durantion
            cell.btnShowOption.isHidden = true
            cell.index = indexPath.row
            if dataForCell.artwork == nil {
                cell.imgVideo.isHidden = true
                cell.imgHidden.isHidden = false
            } else {
                cell.imgVideo.image = dataForCell.artwork
                cell.imgHidden.isHidden = true
                cell.imgVideo.isHidden = false
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.typeListSong {
            playAlbumFromDetail(lstSong: self.listSong, indexPlaying: indexPath.row, isRandom: false)
        } else {
            var lstPlay = [VideoModel]()
            lstPlay = listVideo
            lstPlay.swapAt(0, indexPath.row)
            let videoPlayer = VideoPlayerController.create()
            videoPlayer.dataVideo = lstPlay
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                if let viewPlay = delegate.viewPlay {
                    viewPlay.pauseAndPlay()
                }
                if let tabbar = delegate.tabbar {
                    tabbar.present(videoPlayer, animated: true, completion: nil)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.typeListSong ? 70 : 80
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if self.typeListSong {
                self.listSong.remove(at: indexPath.row)
                self.tableView.reloadData()
                deleteSongInAlbum(indexItem, indexPath.row)
                numberSong.text = String.init(format: LocalizedString("count_songs", comment: ""), self.listSong.count)
            } else {
                self.listVideo.remove(at: indexPath.row)
                self.tableView.reloadData()
                deleteSongInAlbum(indexItem, indexPath.row)
                numberSong.text = String.init(format: LocalizedString("count_videos", comment: ""), self.listSong.count)
            }
        }
    }
}
extension DetailAlbumController : CustomAlertDetailSongDelegate {
    func addToAlbumTapped(controler: UIViewController, _ index: Int, _ type: TypeShow) {
    }
    
    func editNameTapped(controler: UIViewController, _ index: Int, _ type: TypeShow) {
        let alert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertView") as! CustomAlertView
        alert.providesPresentationContextTransitionStyle = true
        alert.definesPresentationContext = true
        alert.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        alert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        alert.delegate = self
        alert.currentNameAlbum = self.itemAlbum.name
        alert.currentType = .UPDATE
        customAlert.present(alert, animated: true, completion: nil)
    }
    
    func deleteTapped(controler: UIViewController, _ index: Int, _ type: TypeShow) {
        if type == .ALBUM {
            removeAlbum(index)
            dismissDetail()
        }
    }
}
extension DetailAlbumController : CustomAlertViewDelegate {
    func okButtonTapped(textFieldValue: String, typeVideo: Bool) {
        nameAlbum.text = textFieldValue
        renameAlbum(indexItem, textFieldValue)
        customAlert.titleLabel.text = textFieldValue
    }
    
    func cancelButtonTapped() {
    }
}
