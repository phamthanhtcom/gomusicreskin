//
//  ListAlbumController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/10/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
enum TypeDisplay {
    case WHENVIEW
    case WHENADDSONG
}
class ListAlbumController: UIViewController {

    @IBOutlet weak var nameList: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    
    let cellListAlbum = "NewItemAlbumCell"
    var listAlbum = [AlbumModel]()
    var typeDisplay : TypeDisplay = .WHENVIEW
    var pathToAdd = [String]()
    var typeAddVideo = false
    var showBarMini = false {
        didSet {
            self.collectionView.contentInset = showBarMini ? UIEdgeInsets.init(top: 16, left: 0, bottom: 110, right: 0) : UIEdgeInsets.init(top: 16, left: 0, bottom: 10, right: 0)
        }
    }
    
    class func create() -> ListAlbumController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! ListAlbumController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            showBarMini = delegate.isShowBarMini
        } else {
            showBarMini = false
        }
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: cellListAlbum, bundle: nil), forCellWithReuseIdentifier: cellListAlbum)
        NotificationCenter.default.addObserver( self, selector: #selector(hideViewPlay), name: NSNotification.Name("HIDE_VIEW_PLAY"), object: nil )
        NotificationCenter.default.addObserver( self, selector: #selector(showViewPlay), name: NSNotification.Name("SHOW_VIEW_PLAY"), object: nil )
    }
    
    @objc func updateData(){
        listAlbum = getListAlbum()
        collectionView.reloadData()
    }
    
    @objc func hideViewPlay() {
        showBarMini = false
    }
    
    @objc func showViewPlay() {
        showBarMini = true
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let angle = typeDisplay == .WHENADDSONG ? -.pi/2 : 0.0
        let tr = CGAffineTransform.identity.rotated(by: CGFloat(angle))
        UIView.animate(withDuration: 0.3, animations: {
            self.btnBack.transform = tr
        })
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        switch typeDisplay {
        case .WHENVIEW:
            self.navigationController?.popViewController(animated: true)
        default:
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func actionAddAlbum(_ sender: Any) {
        showFormAddAlbum()
    }
    
    func showFormAddAlbum(){
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertView") as! CustomAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.currentType = .NEW
        self.present(customAlert, animated: true, completion: nil)
    }
}

extension ListAlbumController : CustomAlertViewDelegate {
    func okButtonTapped(textFieldValue: String, typeVideo: Bool) {
        if textFieldValue.trimmingCharacters(in: .whitespaces) != "" {
            let newAlbum = AlbumModel(name: textFieldValue, listSong: [], isVideo: typeVideo)
            createNewsAlbum(newAlbum)
        }
    }
    
    func cancelButtonTapped() {
    }
}

extension ListAlbumController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listAlbum.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellListAlbum, for: indexPath) as! NewItemAlbumCell
        cell.nameAlbum.text = listAlbum[indexPath.item].name
        cell.imgAlbum.image = listAlbum[indexPath.item].isVideo ? UIImage(named: "multimedia") : UIImage(named: "music-player")
        cell.subTitle.text = String.init(format: listAlbum[indexPath.item].isVideo ? LocalizedString("count_videos", comment: "") : LocalizedString("count_songs", comment: ""), listAlbum[indexPath.item].listSong.count)
        switch typeDisplay {
        case .WHENADDSONG:
            if self.typeAddVideo {
                if listAlbum[indexPath.item].isVideo {
                    cell.contentView.alpha = 1
                } else {
                    cell.contentView.alpha = 0.4
                }
            } else {
                if !listAlbum[indexPath.item].isVideo {
                    cell.contentView.alpha = 1
                } else {
                    cell.contentView.alpha = 0.4
                }
            }
        default:
            cell.contentView.alpha = 1
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch typeDisplay {
        case .WHENVIEW:
            let detail = DetailAlbumController.create()
            detail.itemAlbum = listAlbum[indexPath.item]
            detail.indexItem = indexPath.item
            detail.typeListSong = !listAlbum[indexPath.item].isVideo
            detail.showBarMini = self.showBarMini
            self.navigationController?.pushViewController(detail, animated: true)
        default:
            if typeAddVideo {
                if self.listAlbum[indexPath.item].isVideo {
                    addListSongToAlbum(indexPath.item, pathToAdd)
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                if !self.listAlbum[indexPath.item].isVideo {
                    addListSongToAlbum(indexPath.item, pathToAdd)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthItem: CGFloat = collectionView.frame.width / 2
        let heightItem: CGFloat = (widthItem * 122 / 156) + 82
        return CGSize.init(width: widthItem, height: heightItem)
    }
}
