//
//  ImportController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/2/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import SwiftyDropbox
import GoogleSignIn
import GoogleAPIClientForREST
import MSAL
import MobileCoreServices
import GTMSessionFetcher
class ImportController: UIViewController {
    @IBOutlet weak var btnWfifi: CustomImportItem!
    @IBOutlet weak var btnDropbox: CustomImportItem!
    @IBOutlet weak var btnGGDriver: CustomImportItem!
    @IBOutlet weak var btnOneDriver: CustomImportItem!
    @IBOutlet weak var btnICloud: CustomImportItem!
    @IBOutlet weak var btnMore: CustomImportItem!
    
    fileprivate let service = GTLRDriveService()
    
    let kScopes: [String] = ["https://graph.microsoft.com/user.read", "https://graph.microsoft.com/files.read.all"]
    var accessToken = String()
    var applicationContext : MSALPublicClientApplication?
    var webviewParameters : MSALWebviewParameters?
    var isSilentGGDriver = false
    var isSilentDropbox = false
    
    class func create() -> ImportController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! ImportController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addingObserver()
        settingView()
        //Google Driver
        setupGoogleSignIn()
        //Setup OneDriver
        configLoginOneDrive()
    }
    //===dropbox====//
    func addingObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(getEmailDropbox), name: NSNotification.Name(rawValue: "DROPBOX_AUTHEN"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeState), name: NSNotification.Name(rawValue: "DROPBOX_AUTHEN_SILENT"), object: nil)
        if self.isAuthentDropbox() {
            getEmailDropbox()
        }
    }
    //===end dropbox===//
    
    //===google driver===//
    
    private func setupGoogleSignIn() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().scopes = [kGTLRAuthScopeDriveReadonly]
        
        if GIDSignIn.sharedInstance().hasPreviousSignIn() {
            isSilentGGDriver = true
            GIDSignIn.sharedInstance().restorePreviousSignIn()
        } else {
            print("NEED LOGIN")
        }
    }
    
    //===end google driver//
    
    func settingView(){
        setTitleButton("Computer", "", btnWfifi)
        setTitleButton("Dropbox", "Tap to login", btnDropbox)
        setTitleButton("GG Driver", "Tap to login", btnGGDriver)
        setTitleButton("OneDriver", "Tap to login", btnOneDriver)
        setTitleButton("ICloud", "", btnICloud)
        setTitleButton("More Tools", "", btnMore)
        //Handle click custom button
        btnDropbox.handlerLogin = {
            item in
            if self.isAuthentDropbox() {
                let dropFile = DropboxFileController.create()
                self.presentDetail(dropFile)
            } else {
                self.signInDropBox()
            }
        }
        btnGGDriver.handlerLogin = {
            item in
            if !(GIDSignIn.sharedInstance()?.hasPreviousSignIn())! {
                GIDSignIn.sharedInstance().signIn()
            } else {
                let driveFile = DriveFileController.create()
                driveFile.service = self.service
                self.presentDetail(driveFile)
            }
        }
        btnOneDriver.handlerLogin = {
            item in
            if self.currentAccount() == nil {
                self.acquireTokenInteractively()
            } else {
                let oneDriver = OneDriverFileController.create()
                oneDriver.accessToken = self.accessToken
                self.presentDetail(oneDriver)
            }
        }
        
        btnWfifi.handlerLogin = {
            item in
            let tranfer = TranferController.create()
            self.presentDetail(tranfer)
        }
        
        btnICloud.handlerLogin = {
            item in
            let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypeItem as String], in: UIDocumentPickerMode.import)
            documentPicker.delegate = self
            documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            self.present(documentPicker, animated: true, completion: nil)
        }
        
        btnMore.handlerLogin = {
            item in
            openURL(destURL: Constants.APP_MORE_STORE_URL)
        }
    }
    
    func setTitleButton(_ firstTitle : String , _ subTitle : String, _ button : CustomImportItem ){
        button.delegate = self
        button.lbImport.text = firstTitle
        button.subTitle.text = subTitle
    }
}
extension ImportController : UIDocumentPickerDelegate {
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        
        if !isMusicURL(myURL) {
            return
        } else {
            if isSongExists(musicName: myURL.lastPathComponent) {
                showAlertWith(title: LocalizedString("title_error", comment: ""), message: LocalizedString("msg_song_exist", comment: ""))
                return
            } else {
                let name = myURL.lastPathComponent
                let destURL = URL.init(fileURLWithPath: getDirRootMusicPath() + "/\(name)")
                do {
                    try FileManager.default.moveItem(at: myURL, to: destURL)
                    NotificationCenter.default.post(name: Notification.Name(Constants.RELOAD_LIST_SONG), object: nil, userInfo: nil)
                    showAlertWith(title: LocalizedString("download", comment: ""), message: LocalizedString("msg_download_success", comment: ""))
                } catch (let writeError) {
                    print("Error store file \(String(describing: destURL.path)) : \(writeError)")
                }
            }
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {}
}
extension ImportController : SignOutDelegate{
    func signOutWithType(_ type: String) {
        switch type {
        case LocalizedString("dropbox", comment: ""):
            let alert = UIAlertController(title: "Log Out", message: "Do you want to logout DropBox?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                DropboxClientsManager.unlinkClients()
                self.setTitleButton(LocalizedString("dropbox", comment: ""), LocalizedString("tap_login", comment: "") , self.btnDropbox)
                self.btnDropbox.btnSignOut.isHidden = true
                self.navigationController?.popToRootViewController(animated: true)
            }))
            alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            break
        case LocalizedString("google_driver", comment: ""):
            let alert = UIAlertController(title: "Log Out", message: "Do you want to logout Google Drive?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                if let instance = GIDSignIn.sharedInstance(){
                    instance.disconnect()
                }
                self.navigationController?.popToRootViewController(animated: true)
            }))
            alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            break
        case LocalizedString("one_driver", comment: ""):
            let alert = UIAlertController(title: "Log Out", message: "Do you want to logout One Drive?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                guard let applicationContext = self.applicationContext else { return }
                
                guard let account = self.currentAccount() else { return }
                guard let webviewParameters = self.webviewParameters else { return }
                let signoutParameters = MSALSignoutParameters(webviewParameters: webviewParameters)
                signoutParameters.signoutFromBrowser = false
                applicationContext.signout(with: account, signoutParameters: signoutParameters, completionBlock: { (success, error) in
                    if let error = error {
                        print("Couldn't sign out account with error: \(error)")
                        return
                    }
                    if success {
                        do {
                            self.accessToken = ""
                            try applicationContext.remove(account)
                            self.setTitleButton(LocalizedString("one_driver", comment: ""), LocalizedString("tap_login", comment: "") , self.btnOneDriver)
                            self.btnOneDriver.btnSignOut.isHidden = true
                            print("Sign out completed successfully")
                        } catch let error as NSError {
                            print("Received error signing account out: \(error)")
                        }
                    }
                })
            }))
            alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
}
//One Driver method
extension ImportController {
    func configLoginOneDrive(){
        do {
            guard let authorityURL = URL(string: Constants.kAuthority) else {
                print("Unable to create authority URL")
                return
            }
            self.webviewParameters = MSALWebviewParameters(authPresentationViewController: self)
            let authority = try MSALAuthority(url: authorityURL)
            let config = MSALPublicClientApplicationConfig.init(clientId: Constants.CLIENT_ID_ONEDRIVE, redirectUri: Constants.kRedirectUri, authority: authority)
            
            self.applicationContext = try MSALPublicClientApplication(configuration: config)
            
            if self.currentAccount() == nil {
                //Do nothing
                print("Account nil")
            } else {
                self.acquireTokenSilently()
            }
        } catch let error {
            print("Unable to create Application Context \(error)")
        }
    }
    
    func currentAccount() -> MSALAccount? {
        
        guard let applicationContext = self.applicationContext else { return nil }
        
        do {
            let cachedAccounts = try applicationContext.allAccounts()
            
            if !cachedAccounts.isEmpty {
                return cachedAccounts.first
            }
        } catch let error as NSError {
            print("Didn't find any accounts in cache: \(error)")
        }
        
        return nil
    }
    
    func acquireTokenInteractively() {
        guard let applicationContext = self.applicationContext else { return }
        guard let webviewParameters = self.webviewParameters else { return }

        let params = MSALInteractiveTokenParameters(scopes: kScopes, webviewParameters: webviewParameters)
        applicationContext.acquireToken(with: params) { (result, error) in
            if let error = error {
                print("Could not acquire token: \(error)")
                let userInfo = (error as NSError).userInfo
                if let message = userInfo[MSALErrorDescriptionKey] as? String {
                    print(message)
                    return
                }
            }
            
            guard let result = result else {
                print("Could not acquire token: No result returned")
                return
            }
            
            self.accessToken = result.accessToken
            let account = result.account
            DispatchQueue.main.async {
                if let userName = account.username {
                    self.setTitleButton(LocalizedString("one_driver", comment: ""), userName, self.btnOneDriver)
                }
                self.btnOneDriver.btnSignOut.isHidden = false
                self.showToast(msg: String.init(format: LocalizedString("login", comment: ""), "OneDrive"))
            }
        }
    }
    
    func acquireTokenSilently() {
        guard let applicationContext = self.applicationContext else { return }
        let params = MSALSilentTokenParameters(scopes: kScopes, account: self.currentAccount()!)
        applicationContext.acquireTokenSilent(with: params) { (result, error) in
            if let error = error {
                let nsError = error as NSError
                if (nsError.domain == MSALErrorDomain) {
                    
                    DispatchQueue.main.async {
                        self.acquireTokenInteractively()
                    }
                } else {
                    print("Could not acquire token silently: \(error)")
                    let userInfo = (error as NSError).userInfo
                    if let message = userInfo[MSALErrorDescriptionKey] as? String {
                        print(message)
                        return
                    }
                }
                return
            }
            
            guard let result = result else {
                print("Could not acquire token: No result returned")
                return
            }
            
            self.accessToken = result.accessToken
            let account = result.account
            DispatchQueue.main.async {
                if let userName = account.username {
                    self.setTitleButton(LocalizedString("one_driver", comment: ""), userName, self.btnOneDriver)
                }
                self.btnOneDriver.btnSignOut.isHidden = false
            }
        }
    }
}
//End one driver method

extension ImportController{
    //===dropbox method====//
    @objc func changeState(){
        self.isSilentDropbox = false
    }
    
    @objc func getEmailDropbox(){
        if let client = DropboxClientsManager.authorizedClient{
            let user = client.users.getCurrentAccount()
            user.response { (accountData, error) in
                if let error = error {
                    print(error.description)
                    return
                }
                if let accountData = accountData {
                    DispatchQueue.main.async {
                        self.setTitleButton("Dropbox", accountData.email , self.btnDropbox)
                        self.btnDropbox.btnSignOut.isHidden = false
                        if !self.isSilentDropbox{
                            self.showToast(msg: String.init(format: LocalizedString("login", comment: ""), "Dropbox"))
                        }
                    }
                }
            }
        }
    }
    
    func isAuthentDropbox() -> Bool{
        if DropboxClientsManager.authorizedClient != nil {
            isSilentDropbox = true
            return true
        }
        return false
    }
    
    func signInDropBox() {
//        DropboxClientsManager.authorizeFromController(UIApplication.shared,
//                                                      controller: self,
//                                                      openURL: { (url: URL) -> Void in
//                                                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        })
        let scopeRequest = ScopeRequest(scopeType: .user, scopes: ["files.content.read", "account_info.read", "files.metadata.read"], includeGrantedScopes: true)
        DropboxClientsManager.authorizeFromControllerV2(
            UIApplication.shared,
            controller: self,
            loadingStatusDelegate: nil,
            openURL: { (url: URL) -> Void in
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            },
            scopeRequest: scopeRequest
        )
    }
    //===end dropbox method===//
}
extension ImportController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let err = error {
            service.authorizer = nil
            showAlertWith(title: LocalizedString("title_warning", comment: ""), message: err.localizedDescription)
        } else {
            service.authorizer = user.authentication.fetcherAuthorizer()
            self.setTitleButton(LocalizedString("google_driver", comment: ""), user.profile.email, btnGGDriver)
            self.btnGGDriver.btnSignOut.isHidden = false
            if !isSilentGGDriver {
               self.showToast(msg: String.init(format: LocalizedString("login", comment: ""), "GoogleDriver"))
            }
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        if let err = error {
            showAlertWith(title: LocalizedString("title_warning", comment: ""), message: err.localizedDescription)
        } else {
            service.authorizer = nil
            self.setTitleButton(LocalizedString("google_driver", comment: ""), LocalizedString("tap_login", comment: "") , self.btnGGDriver)
            self.btnGGDriver.btnSignOut.isHidden = true
        }
    }
}
