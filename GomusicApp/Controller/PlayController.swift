//
//  PlayController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/2/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import FloatingPanel

class PlayController: UIViewController {
    
    @IBOutlet weak var viewPlay: UIView!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var timeLock: UILabel!
    @IBOutlet weak var nameSong: UILabel!
    @IBOutlet weak var nameArtist: UILabel!
    
    class func create() -> PlayController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! PlayController
    }
    
    fileprivate func settingTableView() {
        imgBlur.addBlurEffect()
//        imgPlay.layer.cornerRadius = imgPlay.bounds.height / 2
//        viewPlay.layer.cornerRadius = viewPlay.bounds.height / 2
//        viewPlay.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingTableView()
    }
}
extension PlayController : UpdateSong {
    func updateUI(_ item: AudioItem) {
        if let title = item.title {
            self.nameSong.text = title
        } else {
            self.nameSong.text = LocalizedString("unknow", comment: "")
        }
        if let artist = item.artist {
            self.nameArtist.text = artist
        } else {
            self.nameArtist.text = LocalizedString("unknow", comment: "")
        }
        if let artwork = item.artworkImage {
            self.imgPlay.image = artwork
            self.imgBlur.image = artwork
        } else {
            self.imgPlay.image = UIImage(named: "")
        }
    }
}
