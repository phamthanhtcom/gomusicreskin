//
//  ListSongPlayingController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 9/12/20.
//  Copyright © 2020 nguyen hong doan. All rights reserved.
//

import UIKit
protocol ListSongPlayingDelegate: class {
    func onClickItem(_ control: UIViewController ,_ index: Int, _ itemData: MusicModel)
}

class ListSongPlayingController: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tabView: UITableView!
    @IBOutlet weak var titleName: UILabel!
    weak var delegate: ListSongPlayingDelegate?
    
    let nameSongCell = "ItemSongCell"
    var listSong = [MusicModel]()
    
    class func create() -> ListSongPlayingController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! ListSongPlayingController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        tabView.delegate = self
        tabView.dataSource = self
        tabView.register(UINib(nibName: nameSongCell, bundle: nil), forCellReuseIdentifier: nameSongCell)
        self.loadData()
    }
    
    func loadData() {
        self.tabView.reloadData()
    }
    @IBAction func backTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ListSongPlayingController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listSong.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nameSongCell, for: indexPath) as! ItemSongCell
        cell.nameSong.text = listSong[indexPath.row].nameSong
        cell.artistName.text = listSong[indexPath.row].artist
        cell.btnShowOption.isHidden = true
        cell.index = indexPath.row
        if listSong[indexPath.row].artwork == nil {
            cell.imgSong.isHidden = true
            cell.imgHidden.isHidden = false
        } else {
            cell.imgSong.image = listSong[indexPath.row].artwork
            cell.imgHidden.isHidden = true
            cell.imgSong.isHidden = false
        }
        if listSong[indexPath.row].isPlay {
            cell.indicator.isHidden = false
            cell.indicator.startAnimating()
        } else {
            cell.indicator.stopAnimating()
            cell.indicator.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.onClickItem(self, indexPath.row, self.listSong[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
