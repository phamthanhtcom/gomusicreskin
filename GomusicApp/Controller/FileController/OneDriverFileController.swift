//
//  OneDriverFileController.swift
//  GomusicApp
//
//  Created by mac on 8/18/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import MSAL
import Alamofire
import SVProgressHUD
class OneDriverFileController: BaseTableController {
    @IBOutlet weak var lblRootName: UILabel!
    
    var itemId = "root"
    var folderName = "Root"
    var accessToken : String!
    var lstOneFiles = [OneDriveModel]()
    
    class func create() -> OneDriverFileController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! OneDriverFileController
    }
    
    override func viewDidLoad() {
        tag = Constants.ONE_DRIVER_VC
        super.viewDidLoad()
        lblRootName.text = folderName
        getFiles()
    }
    
    func getFiles(){
        SVProgressHUD.show()
        getContentWithToken{ (lst) in
            DispatchQueue.main.async {
                self.lstOneFiles = lst
                self.lstItem = lst
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
        }
    }

    func getContentWithToken(_ completion: @escaping ([OneDriveModel]) -> Void) {
        let url = String.init(format: Constants.kGraphURI, itemId)
        let header: HTTPHeaders = ["Authorization": "Bearer \(self.accessToken ?? ""))"]
        Alamofire.request(url, method: .get, parameters: [:], headers: header).responseJSON {
            response in
            var lst = [OneDriveModel]()
            if let jsonData = response.result.value as? [String: Any] {
                if let values = jsonData["value"] as? [[String: Any]] {
                    for value in values {
                        // Get onedrive model
                        let one = OneDriveModel()
                        one.initFromDic(value)
                        lst.append(one)
                    }
                }
            }
            completion(lst)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.lstOneFiles[indexPath.row]
        if item.isFolder {
            let oneDrive = OneDriverFileController.create()
            oneDrive.accessToken = self.accessToken
            oneDrive.itemId = String.init(format: "items/%@", item.id)
            oneDrive.folderName = item.name
            self.presentDetail(oneDrive)
        } else if isMusicFile(item){
            downloadFile(item)
        } else if isVideoFile(item) {
            downloadFile(item)
        } else {
            print("Do nothing")
        }
    }
    
    func downloadFile(_ file: OneDriveModel) {
        
        let name = file.name
        if isSongExists(musicName: name) {
            showAlertWith(title: LocalizedString("title_error", comment: ""), message: LocalizedString("msg_song_exist", comment: ""))
            return
        }
        if isCanDownloadVideo(bytes: file.size) {
            SVProgressHUD.show()
            let destURL = URL.init(fileURLWithPath: getDirRootMusicPath() + "/\(name)")
            
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                return (destURL, [.removePreviousFile, .createIntermediateDirectories])
            }
            let mess = String.init(format: LocalizedString("msg_download_start", comment: ""), file.name)
            self.view.makeToast(mess, duration: 3, position: .center)
            let queue = DispatchQueue(label: "fetchNewsFile", qos: .background, attributes: .concurrent)
            Alamofire.download(file.dowloadUrl, to: destination).downloadProgress(queue: queue, closure: { (progress) in
                print(progress.fractionCompleted)
            }).responseData(queue: queue) { (response) in
                if response.error == nil, let path = response.destinationURL {
                    print(path)
                    DispatchQueue.main.async {
                        if let topController = topMostController() {
                            topController.view.makeToast(LocalizedString("msg_download_success", comment: ""), duration: 3, position: .center)
                        }
                        NotificationCenter.default.post(name: Notification.Name(Constants.RELOAD_LIST_SONG), object: nil, userInfo: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
                        updateBadge()
                    }
                } else {
                    if let topController = topMostController() {
                        topController.showAlertWith(title: LocalizedString("title_error", comment: ""), message: LocalizedString("msg_download_err", comment: ""))
                    }
                }
            }
        } else {
            self.showAlertWith(title: LocalizedString("title_error", comment: ""), message: String.init(format: LocalizedString("over_size_download", comment: ""), Constants.MAX_VIDEO_SIZE))
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.dismissDetail()
    }
    
}
