//
//  DriveFileController.swift
//  GomusicApp
//
//  Created by mac on 8/13/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import GoogleAPIClientForREST
import SVProgressHUD
import GTMSessionFetcher
import Alamofire
import Toast_Swift

class DriveFileController: BaseTableController {
    
    @IBOutlet weak var lblRootName: UILabel!
    
    var service : GTLRDriveService!
    
    var rootName = "Root"
    var rootId = "root"
    var lstDrives = [GTLRDrive_File]()
    var fetcher: GTMSessionFetcher?
    var downloadfile: GTLRDrive_File?
    
    class func create() -> DriveFileController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! DriveFileController
    }
    
    override func viewDidLoad() {
        tag = Constants.GG_DRIVE_VC
        super.viewDidLoad()
        lblRootName.text = rootName
        getFiles()
    }
    
    func getFiles(){
        SVProgressHUD.show()
        getGGDriveFiles { (lst) in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.lstDrives = lst
                self.lstItem = lst
                self.tableView.reloadData()
            }
        }
    }
    
    private func getGGDriveFiles(_ completion: @escaping ([GTLRDrive_File]) -> Void) {
        let query = GTLRDriveQuery_FilesList.query()
        query.fields = "files(id,name,mimeType,modifiedTime,createdTime,fileExtension,size,kind,iconLink,thumbnailLink),nextPageToken"
        query.q = "trashed = false and '\(rootId)' In parents"
        
        service.executeQuery(query) { (ticket, files, err) in
            if let fileList = files as? GTLRDrive_FileList {
                guard let files = fileList.files else {
                    completion([])
                    return
                }
                completion(files)
            } else {
                completion([])
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.lstDrives[indexPath.row]
        if isFolder(item) {
            let dropFile = DriveFileController.create()
            dropFile.service = self.service
            dropFile.rootId = item.identifier!
            dropFile.rootName = item.name!
            self.presentDetail(dropFile)
        } else if isMusicFile(item) {
            downloadGGFile(item)
        } else if isVideoFile(item) {
            downloadGGFile(item)
        } else {
            print("Do nothing")
        }
    }
    
    func downloadGGFile(_ file: GTLRDrive_File) {
        let name = file.name ?? ""
        if isSongExists(musicName: name) {
            showAlertWith(title: LocalizedString("title_error", comment: ""), message: LocalizedString("msg_song_exist", comment: ""))
            return
        }
        guard let fileSize = file.size else {
            return
        }
        if isCanDownloadVideo(bytes: fileSize.int64Value) {
            self.downloadfile = file
            if fetcher != nil && (fetcher?.isFetching)! {
                fetcher?.stopFetching()
            }
            
            var query: GTLRQuery?
            query = GTLRDriveQuery_FilesGet.queryForMedia(withFileId: file.identifier!)
            
            let destURL = URL.init(fileURLWithPath: getDirRootMusicPath() + "/\(name)")
            let downloadRequest = self.service.request(for: query!) as URLRequest
            self.fetcher = self.service.fetcherService.fetcher(with: downloadRequest)
            self.fetcher?.comment = file.name
            self.fetcher?.destinationFileURL = destURL
            self.fetcher?.useBackgroundSession = true
            let mess = String.init(format: LocalizedString("msg_download_start", comment: ""), file.name!)
            self.view.makeToast(mess, duration: 3, position: .center)
            self.fetcher?.beginFetch(completionHandler: { (data, err) in
                self.downloadfile = nil
                DispatchQueue.main.async {
                    if (err == nil) {
                        if let topController = topMostController() {
                            topController.view.makeToast(LocalizedString("msg_download_success", comment: ""), duration: 3, position: .center)
                        }
                        NotificationCenter.default.post(name: Notification.Name(Constants.RELOAD_LIST_SONG), object: nil, userInfo: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
                        updateBadge()
                    } else {
                        if let topController = topMostController() {
                            topController.showAlertWith(title: LocalizedString("title_error", comment: ""), message: LocalizedString("msg_download_err", comment: ""))
                        }
                    }
                }
            })
        } else {
            self.showAlertWith(title: LocalizedString("title_error", comment: ""), message: String.init(format: LocalizedString("over_size_download", comment: ""), Constants.MAX_VIDEO_SIZE))
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.dismissDetail()
    }
    
}
