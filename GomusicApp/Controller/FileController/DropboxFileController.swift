//
//  DropboxFileController.swift
//  GomusicApp
//
//  Created by mac on 8/13/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import SwiftyDropbox
import SVProgressHUD
class DropboxFileController: BaseTableController {
    @IBOutlet weak var lblPath: UILabel!
    
    var dropboxPath = ""
    var rootName = "Root"
    var lstDropbox = [Files.Metadata]()
    
    class func create() -> DropboxFileController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! DropboxFileController
    }
    
    override func viewDidLoad() {
        tag = Constants.DROP_BOX_VC
        super.viewDidLoad()
        lblPath.text = rootName
        getFiles()
    }
    
    func getFiles(){
        SVProgressHUD.show()
        getAllDropboxFiles { (lst) in
            DispatchQueue.main.async {
                self.lstDropbox = lst
                self.lstItem = lst
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func getAllDropboxFiles(_ completion: @escaping ([Files.Metadata]) -> Void) {
        if let client = DropboxClientsManager.authorizedClient {
            client.files.listFolder(path: dropboxPath).response(queue: DispatchQueue(label: "MyDropboxQueue")) { response, error in
                if let result = response {
                    // Parse content
                    let lst = result.entries
                    completion(lst)
                } else {
                    completion([])
                }
            }
        } else {
            completion([])
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.lstDropbox[indexPath.row]
        if item is Files.FolderMetadata {
            let dropFile = DropboxFileController.create()
            dropFile.dropboxPath = item.pathLower!
            dropFile.rootName = item.name
            self.presentDetail(dropFile)
        } else if isMusicFile(item) {
            if let item = item as? Files.FileMetadata {
                downloadDropbox(item)
            }
        } else if isVideoFile(item) {
            if let item = item as? Files.FileMetadata {
                downloadDropbox(item)
            }
        } else {
            print("Do nothing")
        }
        
    }
    
    func downloadDropbox(_ file: Files.FileMetadata) {
        guard let client = DropboxClientsManager.authorizedClient else{
            return
        }
        
        // Download to URL
        let name = file.name
        
        if isSongExists(musicName: name) {
            showAlertWith(title: LocalizedString("title_error", comment: ""), message: LocalizedString("msg_song_exist", comment: ""))
            return
        }
        
        if isCanDownloadVideo(bytes: Int64(file.size)) {
            let destURL = URL.init(fileURLWithPath: getDirRootMusicPath() + "/\(name)")
            let destination: (URL, HTTPURLResponse) -> URL = { temporaryURL, response in
                return destURL
            }
            let mess = String.init(format: LocalizedString("msg_download_start", comment: ""), file.name)
            self.view.makeToast(mess, duration: 3, position: .center)
            let queue = DispatchQueue(label: "fetchNewsFileDropbox", qos: .background, attributes: .concurrent)
            client.files.download(path: file.pathLower!, overwrite: true, destination: destination).response(queue: queue) { (response, error) in
                if let response = response {
                    // Print file info
                    print(response.0)
                    // Print URL
                    print(response.1)
                    DispatchQueue.main.async {
                        if let topController = topMostController() {
                            topController.view.makeToast(LocalizedString("msg_download_success", comment: ""), duration: 3, position: .center)
                        }
                        NotificationCenter.default.post(name: Notification.Name(Constants.RELOAD_LIST_SONG), object: nil, userInfo: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
                        updateBadge()
                    }
                } else if let error = error {
                    print(error.description)
                    DispatchQueue.main.async {
                        if let topController = topMostController() {
                            topController.showAlertWith(title: LocalizedString("title_error", comment: ""), message: LocalizedString("msg_download_err", comment: ""))
                        }
                    }
                }
            }.progress { (progressData) in
                print(progressData.fractionCompleted)
            }
        } else {
            self.showAlertWith(title: LocalizedString("title_error", comment: ""), message: String.init(format: LocalizedString("over_size_download", comment: ""), Constants.MAX_VIDEO_SIZE))
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.dismissDetail()
    }
    
    @IBAction func checkAllMusicTapped(_ sender: Any) {
    }
    
    @IBAction func downloadTapped(_ sender: Any) {
    }
    
}

