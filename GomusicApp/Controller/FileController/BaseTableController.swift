//
//  BaseTableController.swift
//  GomusicApp
//
//  Created by mac on 8/13/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import SwiftyDropbox
import GoogleAPIClientForREST
let reuseIdentifier = "FileCell"
class BaseTableController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var tag = ""
    var lstItem = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tag {
        case Constants.DROP_BOX_VC:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! FileCell
            let item = lstItem[indexPath.row] as! Files.Metadata
            cell.dropFile = item
            cell.fetchInforDrop()
            return cell
        case Constants.GG_DRIVE_VC:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! FileCell
            let item = lstItem[indexPath.row] as! GTLRDrive_File
            cell.ggFile = item
            cell.fetchInforGoogle()
            return cell
        case Constants.ONE_DRIVER_VC:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! FileCell
            let item = lstItem[indexPath.row] as! OneDriveModel
            cell.oneFile = item
            cell.fetchInforOneDrive()
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.00001
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
