//
//  LibraryController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/2/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class LibraryController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var optionAction: ViewOption?
    
    let nameSongCell = "ItemSongCell"
    let nameListAlbumCell = "ListAlbumCell"
    let cellHeader = "HeaderLibraryCell"
    let nameVideoCell = "ItemVideoCell"
    var dataMusic = [MusicModel]()
    var dataVideo = [VideoModel]()
    var dataAlbum = [AlbumModel]()
    var dialogEditSong: CustomAlertDetailSong?
    var dialogEditVideo: CustomAlertDetailSong?
    var isEditSong = false {
        didSet {
            if self.isEditSong {
                self.showViewOption(.SONGS)
            } else {
                self.hideViewOption()
            }
        }
    }
    var isEditVideo = false {
        didSet {
            if self.isEditVideo {
                self.showViewOption(.VIDEO)
            } else {
                self.hideViewOption()
            }
        }
    }
    var listSongChecked = [Int]()
    var listVideoChecked = [Int]()
    var showBarMini = false {
        didSet {
            self.tableView.contentInset = showBarMini ? UIEdgeInsets.init(top: 0, left: 0, bottom: 110, right: 0) : UIEdgeInsets.init(top: 0, left: 0, bottom: 10, right: 0)
        }
    }
    private let heightMenu: CGFloat = Device.hasTopNotch ? (65 + 34) : 65
    class func create() -> LibraryController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! LibraryController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: nameSongCell, bundle: nil), forCellReuseIdentifier: nameSongCell)
        tableView.register(UINib(nibName: nameListAlbumCell, bundle: nil), forCellReuseIdentifier: nameListAlbumCell)
        tableView.register(UINib(nibName: cellHeader, bundle: nil), forHeaderFooterViewReuseIdentifier: cellHeader)
        tableView.register(UINib.init(nibName: nameVideoCell, bundle: nil), forCellReuseIdentifier: nameVideoCell)
        NotificationCenter.default.addObserver( self, selector: #selector(hideViewPlay), name: NSNotification.Name("HIDE_VIEW_PLAY"), object: nil )
        NotificationCenter.default.addObserver( self, selector: #selector(showViewPlay), name: NSNotification.Name("SHOW_VIEW_PLAY"), object: nil )
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil)
    }
    
    @objc func updateData(){
        self.loadData()
    }
    
    @objc func hideViewPlay() {
        showBarMini = false
    }
    
    @objc func showViewPlay() {
        showBarMini = true
    }
    
    func loadData() {
        self.dataMusic = getAllFileInFolder()
        self.dataVideo = getAllVideoInFolder()
        self.dataAlbum = getListAlbum()
        self.tableView.reloadData()
    }
    
    func getListMusicChecked() -> [MusicModel] {
        var listCheck = [MusicModel]()
        for (index , item) in dataMusic.enumerated(){
            if listSongChecked.contains(index){
                listCheck.append(item)
            }
        }
        return listCheck
    }
    
    func getListVideoChecked() -> [VideoModel] {
        var listCheck = [VideoModel]()
        for (index, item) in dataVideo.enumerated(){
            if listVideoChecked.contains(index){
                listCheck.append(item)
            }
        }
        return listCheck
    }
}

extension LibraryController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return dataMusic.count
        default:
            return dataVideo.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: nameListAlbumCell, for: indexPath) as! ListAlbumCell
            cell.delegate = self
            cell.loadData(dataAlbum)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: nameSongCell, for: indexPath) as! ItemSongCell
            let dataForCell = dataMusic[indexPath.row]
            cell.nameSong.text = dataForCell.nameSong
            cell.artistName.text = dataForCell.artist
            cell.delegate = self
            cell.index = indexPath.row
            cell.viewCheck.isHidden = !isEditSong
            if listSongChecked.contains(indexPath.row){
                cell.imgCheck.image = UIImage(named: "ic_checked")
            } else {
                cell.imgCheck.image = UIImage(named: "ic_check")
            }
            if dataForCell.artwork == nil {
                cell.imgSong.isHidden = true
                cell.imgHidden.isHidden = false
            } else {
                cell.imgSong.image = dataForCell.artwork
                cell.imgHidden.isHidden = true
                cell.imgSong.isHidden = false
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: nameVideoCell, for: indexPath) as! ItemVideoCell
            let dataForCell = self.dataVideo[indexPath.row]
            cell.nameVideo.text = dataForCell.nameVideo
            cell.artistName.text = dataForCell.artist
            cell.durationVideo.text = dataForCell.durantion
            cell.delegate = self
            cell.index = indexPath.row
            cell.viewCheck.isHidden = !isEditVideo
            if listVideoChecked.contains(indexPath.row) {
                cell.imgCheck.image = UIImage(named: "ic_checked")
            } else {
                cell.imgCheck.image = UIImage(named: "ic_check")
            }
            if dataForCell.artwork == nil {
                cell.imgVideo.isHidden = true
                cell.imgHidden.isHidden = false
            } else {
                cell.imgVideo.image = dataForCell.artwork
                cell.imgHidden.isHidden = true
                cell.imgVideo.isHidden = false
            }
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: cellHeader ) as! HeaderLibraryCell
        headerView.delegate = self
        switch section {
        case 0:
            headerView.btnAdd.setImage(UIImage(named: "add"), for: .normal)
            headerView.btnMenu.setImage(UIImage(named: "menu"), for: .normal)
            headerView.nameHeader.text = "Album"
            headerView.typeList = .ALBUM
        case 1:
            headerView.btnAdd.setImage(!isEditSong ? UIImage(named: "ic_tick") : UIImage(named: "ic_active_check"), for: .normal)
            headerView.btnMenu.setImage(UIImage(named: "ic_type_play"), for: .normal)
            headerView.typeList = .SONGS
            headerView.nameHeader.text = "Songs"
        default:
            headerView.btnAdd.setImage(!isEditVideo ? UIImage(named: "ic_tick") : UIImage(named: "ic_active_check"), for: .normal)
            headerView.btnMenu.setImage(UIImage(named: "ic_type_play"), for: .normal)
            headerView.typeList = .VIDEO
            headerView.nameHeader.text = "Videos"
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 180
        case 1:
            return 70
        default:
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if !isEditSong {
                showBarMini = true
                playAlbumFromDetail(lstSong: self.dataMusic, indexPlaying: indexPath.row, isRandom: false)
                return
            }
            if listSongChecked.contains(indexPath.row) {
                if let indexItem = listSongChecked.firstIndex(of: indexPath.row) {
                    listSongChecked.remove(at: indexItem)
                }
                UIView.performWithoutAnimation {
                    tableView.reloadRows(at: [indexPath], with: .none)
                }
                return
            }
            listSongChecked.append(indexPath.row)
            UIView.performWithoutAnimation {
                tableView.reloadRows(at: [indexPath], with: .none)
            }
        } else if indexPath.section == 2 {
            if !isEditVideo {
                var lstPlay = [VideoModel]()
                lstPlay = dataVideo
                lstPlay.swapAt(0, indexPath.row)
                let videoPlayer = VideoPlayerController.create()
                videoPlayer.dataVideo = lstPlay
                if let delegate = UIApplication.shared.delegate as? AppDelegate {
                    if let viewPlay = delegate.viewPlay {
                        viewPlay.pauseAndPlay()
                    }
                    if let tabbar = delegate.tabbar {
                        tabbar.present(videoPlayer, animated: true, completion: nil)
                    }
                }
                return
            }
            if listVideoChecked.contains(indexPath.row) {
                if let indexItem = listVideoChecked.firstIndex(of: indexPath.row) {
                    listVideoChecked.remove(at: indexItem)
                }
                UIView.performWithoutAnimation {
                    tableView.reloadRows(at: [indexPath], with: .none)
                }
                return
            }
            listVideoChecked.append(indexPath.row)
            UIView.performWithoutAnimation {
                tableView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
}
extension LibraryController : HeaderDelegate {
    func add(_ type: TYPELIST) {
        switch type {
        case .ALBUM:
            //action when click button add header list album
            showFormAddAlbum()
        case .SONGS:
            listSongChecked.removeAll()
            listVideoChecked.removeAll()
            isEditVideo = false
            isEditSong = !isEditSong
            UIView.performWithoutAnimation {
                self.tableView.reloadData()
            }
            //action when click button tick header list songs
        default:
            listVideoChecked.removeAll()
            listSongChecked.removeAll()
            isEditSong = false
            isEditVideo = !isEditVideo
            UIView.performWithoutAnimation {
                self.tableView.reloadData()
            }
            //action when click button tick header list VIDEO
        }
    }
    
    func showFormAddAlbum() {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertView") as! CustomAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.currentType = .NEW
        self.navigationController?.present(customAlert, animated: true, completion: nil)
    }
    
    func showMenu(_ type: TYPELIST) {
        switch type {
        case .ALBUM:
            //action when click button menu header list album
            let listAl = ListAlbumController.create()
            listAl.listAlbum = self.dataAlbum
            listAl.typeDisplay = .WHENVIEW
            self.showBarMini ? NotificationCenter.default.addObserver( self, selector: #selector(showViewPlay), name: NSNotification.Name("SHOW_VIEW_PLAY"), object: nil ) :  NotificationCenter.default.addObserver( self, selector: #selector(showViewPlay), name: NSNotification.Name("HIDE_VIEW_PLAY"), object: nil )
            self.navigationController?.pushViewController(listAl, animated: true)
        case .SONGS:
            showBarMini = true
            playAlbumFromDetail(lstSong: self.dataMusic, indexPlaying: random(), isRandom: true)
        default:
            var lstPlay = [VideoModel]()
            lstPlay = dataVideo
            lstPlay.swapAt(0, randomIndexVideo())
            let videoPlayer = VideoPlayerController.create()
            videoPlayer.dataVideo = lstPlay
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                if let viewPlay = delegate.viewPlay {
                    viewPlay.pauseAndPlay()
                }
                if let tabbar = delegate.tabbar {
                    tabbar.present(videoPlayer, animated: true, completion: nil)
                }
            }
        }
    }
    
    func random() -> Int {
        var randomNumber : Int
        randomNumber = Int(arc4random_uniform(UInt32(self.dataMusic.count - 1)))
        return randomNumber
    }
    
    func randomIndexVideo() -> Int {
        var randomNumber : Int
        randomNumber = Int(arc4random_uniform(UInt32(self.dataVideo.count - 1)))
        return randomNumber
    }
}

extension LibraryController : CustomAlertViewDelegate {
    
    func okButtonTapped(textFieldValue: String, typeVideo: Bool) {
        if textFieldValue.trimmingCharacters(in: .whitespaces) != "" {
            let newAlbum = AlbumModel(name: textFieldValue, listSong: [], isVideo: typeVideo)
            createNewsAlbum(newAlbum)
        }
    }
    
    func cancelButtonTapped() {
    }
}

extension LibraryController : ItemSongCellDelegate {
    func showDetail(_ index: Int) {
        dialogEditSong = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertDetailSong") as? CustomAlertDetailSong
        if let dialog = dialogEditSong {
            dialog.providesPresentationContextTransitionStyle = true
            dialog.definesPresentationContext = true
            dialog.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            dialog.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            dialog.indexItem = index
            dialog.delegate = self
            dialog.infoSong = dataMusic[index]
            dialog.type = .SONG
            self.present(dialog, animated: true, completion: nil)
        }
    }
}

extension LibraryController : ItemVideoCellDelegate {
    func showDetailVideo(_ index: Int) {
        dialogEditVideo = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertDetailSong") as? CustomAlertDetailSong
        if let dialog = dialogEditVideo {
            dialog.providesPresentationContextTransitionStyle = true
            dialog.definesPresentationContext = true
            dialog.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            dialog.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            dialog.indexItem = index
            dialog.delegate = self
            dialog.infoVideo = dataVideo[index]
            dialog.type = .VIDEO
            self.present(dialog, animated: true, completion: nil)
        }
    }
}

extension LibraryController : CustomAlertDetailSongDelegate {
    func addToAlbumTapped(controler: UIViewController, _ index: Int, _ type: TypeShow) {
        controler.dismiss(animated: true) {
            switch type {
            case .SONG:
                let list = ListAlbumController.create()
                list.listAlbum = self.dataAlbum
                list.typeDisplay = .WHENADDSONG
                list.typeAddVideo = false
                var listPath = [String]()
                listPath.append(self.dataMusic[index].path)
                list.pathToAdd = listPath
                self.present(list, animated: true, completion: nil )
            case .VIDEO:
                let list = ListAlbumController.create()
                list.listAlbum = self.dataAlbum
                list.typeDisplay = .WHENADDSONG
                list.typeAddVideo = true
                var listPath = [String]()
                listPath.append(self.dataVideo[index].path)
                list.pathToAdd = listPath
                self.present(list, animated: true, completion: nil )
            default:
                break
            }
        }
    }
    
    func editNameTapped(controler: UIViewController, _ index: Int, _ type: TypeShow) {
    }
    
    func deleteTapped(controler: UIViewController, _ index: Int, _ type: TypeShow) {
        controler.dismiss(animated: true) {
            if type == .SONG {
                deleteFileByPath(self.dataMusic[index].path)
            } else if type == .VIDEO {
                deleteFileByPath(self.dataVideo[index].path)
            }
        }
    }
}

extension LibraryController : ListAlbumCellDelegate {
    func onClickItem(_ index: Int, _ isEmptyList: Bool) {
        if isEmptyList {
            showFormAddAlbum()
        } else {
            let detail = DetailAlbumController.create()
            detail.itemAlbum = dataAlbum[index]
            detail.indexItem = index
            detail.typeListSong = !dataAlbum[index].isVideo
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }
}

extension LibraryController: ViewOptionDelegate {
    func addOption(_ type: TYPELIST) {
        switch type {
        case .SONGS:
            if listSongChecked.isEmpty {
                return
            }
            let listCheck = getListMusicChecked()
            let listPath = listCheck.map{ $0.path }
            listSongChecked.removeAll()
            isEditSong = !isEditSong
            UIView.performWithoutAnimation {
                tableView.reloadData()
            }
            let list = ListAlbumController.create()
            list.listAlbum = dataAlbum
            list.typeDisplay = .WHENADDSONG
            list.typeAddVideo = false
            list.pathToAdd = listPath
            self.present(list, animated: true, completion: nil )
        case .VIDEO:
            if listVideoChecked.isEmpty {
                return
            }
            let listCheck = getListVideoChecked()
            let listPath = listCheck.map{ $0.path }
            listVideoChecked.removeAll()
            isEditVideo = !isEditVideo
            UIView.performWithoutAnimation {
                tableView.reloadData()
            }
            let list = ListAlbumController.create()
            list.listAlbum = dataAlbum
            list.typeAddVideo = true
            list.typeDisplay = .WHENADDSONG
            list.pathToAdd = listPath
            self.present(list, animated: true, completion: nil )
        default:
            break
        }
    }
    
    func deleteOption(_ type: TYPELIST) {
        switch type {
        case .SONGS:
            if listSongChecked.isEmpty {
                return
            }
            let listCheck = getListMusicChecked()
            let listPath = listCheck.map{ $0.path }
            removeListFile(listPath)
            listSongChecked.removeAll()
            isEditSong = !isEditSong
            UIView.performWithoutAnimation {
                tableView.reloadData()
            }
        case .VIDEO:
            if listVideoChecked.isEmpty {
                return
            }
            let listCheck = getListVideoChecked()
            let listPath = listCheck.map{ $0.path }
            removeListFile(listPath)
            listVideoChecked.removeAll()
            isEditVideo = !isEditVideo
            UIView.performWithoutAnimation {
                tableView.reloadData()
            }
        default:
            break
        }
        
    }
    
    func showViewOption(_ type: TYPELIST) {
        if optionAction == nil {
            let y: CGFloat = UIScreen.main.bounds.height - heightMenu - (showBarMini ? 110 : 10) - 120
            let x: CGFloat = UIScreen.main.bounds.width - 15 - 180
            self.optionAction = ViewOption.init(frame: CGRect(x: x, y: y, width: 180, height: 80))
            if let option = self.optionAction {
                option.delegate = self
                option.typeList = type
                self.view.addSubview(option)
            }
        }
    }
    
    func hideViewOption() {
        if let option = optionAction {
            option.removeFromSuperview()
            optionAction = nil
        }
    }
}
