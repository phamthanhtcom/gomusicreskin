//
//  SettingController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/2/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import MessageUI
class SettingController: UIViewController {
    @IBOutlet weak var btnRemoveAds: CustomButtonSetting!
    @IBOutlet weak var btnRateApp: CustomButtonSetting!
    @IBOutlet weak var btnFeedBack: CustomButtonSetting!
    @IBOutlet weak var btnTellFriend: CustomButtonSetting!
    @IBOutlet weak var btnGetMoreTools: CustomButtonSetting!
    
    class func create() -> SettingController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! SettingController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rateApp()
        tellFriend()
        feedBack()
        getMoreTools()
    }
    
    func rateApp(){
        btnRateApp.handlerSetting = {
            item in
            openURL(destURL: Constants.YOUR_APP_STORE_URL)
        }
    }
    
    func tellFriend(){
        btnTellFriend.handlerSetting = {
            item in
            var objectToShare = [String]()
            let msg = String.init(format: LocalizedString("share_friends", comment: ""), Constants.YOUR_APP_STORE_URL)
            objectToShare.append(msg)
            let activityViewController = UIActivityViewController(activityItems: objectToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    func feedBack(){
        btnFeedBack.handlerSetting = {
            item in
            self.sendEmail()
        }
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([Constants.EMAIL_FEEDBACK])
            mail.setMessageBody("Pls write your feedback here !", isHTML: false)
            present(mail, animated: true)
        } else {
        }
    }
    
    func getMoreTools(){
        btnGetMoreTools.handlerSetting = {
            item in
            openURL(destURL: Constants.APP_MORE_STORE_URL)
        }
    }
}
extension SettingController : MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
