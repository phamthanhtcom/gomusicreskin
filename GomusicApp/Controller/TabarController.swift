//
//  ViewController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/2/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class TabarController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var customTab: CustomTabbar!
    
    var selectIndex: Int = 0
    
    lazy var viewControllers: [UIViewController] = {
        return self.prepareControllers()
    }()
    
    class func create() -> TabarController {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: self)) as! TabarController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customTab.delegate = self
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.tabbar = self
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupScrollView()
    }
    
    fileprivate func prepareControllers() -> [UIViewController]  {
        let importController = UINavigationController.init(rootViewController: ImportController.create())
        importController.navigationBar.isHidden = true
        let libraryController = RootLibraryController.create()
        let settingController = RootSettingController.create()
        return [importController, UIViewController(), libraryController, settingController]
        
    }
    
    fileprivate func selectedSegmentioIndex() -> Int {
        return selectIndex
    }
    
    func showPageAtIndex(_ index: Int) {
        if selectIndex == index {
            return
        }
        if index == 1 {
            if let app = UIApplication.shared.delegate as? AppDelegate {
                app.addViewPlay()
                app.hideViewPlay(false)
                selectIndex = 2
                resetBage()
                
                self.view.endEditing(true)
                
                self.customTab.setSelect(atIndex: selectIndex)
                
                let scrollViewWidth = self.scrollView.frame.width
                let contentOffsetX = scrollViewWidth * CGFloat(selectIndex)
                self.scrollView.setContentOffset(
                    CGPoint(x: contentOffsetX, y: 0),
                    animated: true
                )
            }
            return
        }
        
        if index == 3 {
            if self.viewControllers.count >= 3, self.viewControllers[index] is RootSettingController {
                if let controller = self.parent as? MainNavigationDrawerController {
                    if let app = UIApplication.shared.delegate as? AppDelegate {
                        app.hideViewPlay(true)
                    }
                    controller.openRightView()
                }
            }
            return
        }
        
        if let app = UIApplication.shared.delegate as? AppDelegate {
            app.hideViewPlay(index == 0)
        }
        
        selectIndex = index
        if selectIndex == 2 {
            resetBage()
        }
        self.view.endEditing(true)
        
        self.customTab.setSelect(atIndex: index)
        
        let scrollViewWidth = self.scrollView.frame.width
        let contentOffsetX = scrollViewWidth * CGFloat(selectIndex)
        self.scrollView.setContentOffset(
            CGPoint(x: contentOffsetX, y: 0),
            animated: true
        )
    }
    
    // MARK: - Setup container view
    fileprivate func setupScrollView() {
        scrollView.contentSize = CGSize(
            width: UIScreen.main.bounds.width * CGFloat(viewControllers.count),
            height: containerView.frame.height
        )
        
        for (index, viewController) in viewControllers.enumerated() {
            viewController.view.frame = CGRect(
                x: UIScreen.main.bounds.width * CGFloat(index),
                y: 0,
                width: scrollView.frame.width,
                height: scrollView.frame.height
            )
            addChild(viewController)
            scrollView.addSubview(viewController.view, options: .useAutoresize) // module's extension
            viewController.didMove(toParent: self)
        }
    }
    
    // MARK: - Actions
    fileprivate func goToControllerAtIndex(_ index: Int) {
        self.showPageAtIndex(index)
    }
}

extension TabarController: UIScrollViewDelegate, TabSelectDelegate {
    func selectTabAt(index: Int) {
        self.showPageAtIndex(index)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = floor(scrollView.contentOffset.x / scrollView.frame.width)
        self.showPageAtIndex(Int(currentPage))
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 0)
    }
}


