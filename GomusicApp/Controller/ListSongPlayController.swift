//
//  ListSongPlayController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/16/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import MediaPlayer

class ListSongPlayController: UIViewController {

    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var lbDuration: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var btnRandom: UIButton!
    @IBOutlet weak var btnPre: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnRepeat: UIButton!
    @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewCorner: UIView!
    var isPause = false
    let cellSongName = "SongPlayCell"
    var lstSong = [MusicModel]()
    var indexPlaying = -1
    let player = AudioPlayer()
    var currentLength = 0
    var repeatState : RepeatState = .nonRepeat
    var isShuffle = false
    var delegate : UpdateSong?
    var currentAudio : AudioItem?
    var nowPlayingInfor = [String : Any]()
    
    class func create() -> ListSongPlayController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! ListSongPlayController
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    fileprivate func settingTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellSongName, bundle: nil), forCellReuseIdentifier: cellSongName)
        NotificationCenter.default.addObserver(self, selector: #selector(getAllSong), name: NSNotification.Name(rawValue: Constants.RELOAD_LIST_SONG), object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingTableView()
        DispatchQueue.main.async {
            self.viewCorner.roundCorners([.topLeft, .topRight], radius: 20)
            self.viewCorner.clipsToBounds = true
        }
        getAllSong()
        player.delegate = self
        slider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        becomeFirstResponder()
    }
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event != nil{
            switch event!.subtype {
            case .remoteControlPlay, .remoteControlPause:
                pauseAndPlay()
                break
            case .remoteControlPreviousTrack:
                playPreviousSong()
                break
            case .remoteControlNextTrack:
                playNextSong()
                break
            default:
                break
            }
        }
    }
    
    func playAlbum(lstSong : [MusicModel], indexPlay : Int, isRandom : Bool){
        if lstSong.count != 0 {
            self.lstSong = lstSong
            playSong(songName: self.lstSong[indexPlay].path)
            for item in self.lstSong {
                item.isPlay = false
            }
            self.lstSong[indexPlay].isPlay = true
            indexPlaying = indexPlay
            tableView.reloadData()
        }
        if isRandom {
            self.isShuffle = isRandom
            let image = isShuffle ? UIImage(named: "ic_asyns_color") : UIImage(named: "ic_asyns")
            btnRandom.setImage(image, for: .normal)
        }
    }
    
    @objc func getAllSong(){
        self.lstSong = getAllFileInFolder()
        tableView.reloadData()
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent){
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                if !player.state.isStopped {
                    if !player.state.isPaused {
                        player.pause()
                    } else {
                        isPause = true
                    }
                }
                break
            case .moved:
                break
            case .ended:
                if currentLength == 0 {
                    slider.value = 0
                } else {
                    if player.state.isPaused {
                        let timeSeek = Int(slider.value * Float(currentLength))
                        player.seek(to: TimeInterval(timeSeek))
                        if !isPause {
                            player.resume()
                        } else {
                            isPause = false
                        }
                    } else if player.state.isStopped {
                        slider.value = 100
                    }
                }
                break
            default:
                break
            }
        }
    }
    
    @IBAction func shuffleTapped(_ sender: Any) {
        isShuffle = !isShuffle
        let image = isShuffle ? UIImage(named: "ic_asyns_color") : UIImage(named: "ic_asyns")
        btnRandom.setImage(image, for: .normal)
    }
    @IBAction func previousTapped(_ sender: Any) {
        playPreviousSong()
    }
    @IBAction func playAndPauseTapped(_ sender: Any) {
        pauseAndPlay()
    }
    @IBAction func nextTapped(_ sender: Any) {
        playNextSong()
    }
    @IBAction func repeatTapped(_ sender: Any) {
        var current = repeatState.rawValue
        if current == 2 {
            current = 0
        } else {
            current += 1
        }
        repeatState = RepeatState.init(rawValue: current)!
        switch repeatState {
        case .nonRepeat:
            btnRepeat.setImage(UIImage(named: "ic_repeat"), for: .normal)
            break
        case .repeatAll:
            btnRepeat.setImage(UIImage(named: "ic_repeat_color"), for: .normal)
            break
        case .repeatOne:
            btnRepeat.setImage(UIImage(named: "ic_repeat_one"), for: .normal)
            break
        }
    }
    
    
    
    // Music Method
    func playSong(songName : String){
        if let musicPath = getMusicWithURL(songName){
            let mediumURL = URL(fileURLWithPath: musicPath)
            if let item = AudioItem(highQualitySoundURL: nil, mediumQualitySoundURL: mediumURL, lowQualitySoundURL: nil){
                player.play(item: item)
                currentAudio = item
            }
        }
    }
    
    func setEndTime(time: Double) {
        let duration = getTimeString(seconds: Int(time))
        lbDuration.text = duration
    }
    
    func updateTimePlay(time: Double, percent: Float) {
        let duration = getTimeString(seconds: Int(time))
        
        self.startTime.text = duration
        self.slider.value = percent/100.0
    }
    
    func seekMusicToPos(pos: Int){
        if (self.player.state == .playing) {
            self.player.seek(to: TimeInterval(pos))
        }
    }
    
    func pauseAndPlay(){
        switch player.state {
        case .playing:
            player.pause()
            break
        case .paused:
            player.resume()
            break
        case .stopped:
            player.seek(to: 0)
            player.resume()
            break
        default:
            break
        }
    }
    
    func playNextSong(){
        if indexPlaying != -1 {
            self.lstSong[indexPlaying].isPlay = false
            if indexPlaying < self.lstSong.count - 1{
                let temp = indexPlaying + 1
                self.lstSong[temp].isPlay = true
                tableView.reloadRows(at: [IndexPath(row: indexPlaying, section: 0),IndexPath(row: temp, section: 0)], with: .none)
                playSong(songName: self.lstSong[temp].path)
                indexPlaying = temp
            } else {
                self.lstSong[0].isPlay = true
                tableView.reloadRows(at: [IndexPath(row: indexPlaying, section: 0),IndexPath(row: 0, section: 0)], with: .none)
                playSong(songName: self.lstSong[0].path)
                indexPlaying = 0
            }
        }
    }
    
    func playPreviousSong(){
        if indexPlaying != -1 {
            self.lstSong[indexPlaying].isPlay = false
            if indexPlaying > 0 {
                let temp = indexPlaying - 1
                self.lstSong[temp].isPlay = true
                tableView.reloadRows(at: [IndexPath(row: indexPlaying, section: 0),IndexPath(row: temp, section: 0)], with: .none)
                playSong(songName: self.lstSong[temp].path)
                indexPlaying = temp
            } else {
                let temp = self.lstSong.count - 1
                self.lstSong[temp].isPlay = true
                tableView.reloadRows(at: [IndexPath(row: 0, section: 0),IndexPath(row: temp, section: 0)], with: .none)
                playSong(songName: self.lstSong[temp].path)
                indexPlaying = temp
            }
        }
    }
    
    func playRandom(){
        var randomNumber : Int
        repeat {
            randomNumber = Int(arc4random_uniform(UInt32(self.lstSong.count - 1)))
        } while indexPlaying == randomNumber
        self.lstSong[indexPlaying].isPlay = false
        self.lstSong[randomNumber].isPlay = true
        tableView.reloadRows(at: [IndexPath(row: indexPlaying, section: 0),IndexPath(row: randomNumber, section: 0)], with: .none)
        playSong(songName: self.lstSong[randomNumber].path)
        indexPlaying = randomNumber
    }
    
    func replaySong(){
        playSong(songName: self.lstSong[indexPlaying].path)
    }
    
    func updateScreen(){
        if currentAudio != nil {
            nowPlayingInfor[MPMediaItemPropertyTitle] = currentAudio!.title
            nowPlayingInfor[MPMediaItemPropertyArtist] = currentAudio!.artist
            if let image = currentAudio!.artwork {
                nowPlayingInfor[MPMediaItemPropertyArtwork] = image
            }
            
            // Set the metadata
            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfor
        }
    }
}

extension ListSongPlayController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lstSong.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellSongName, for: indexPath) as! SongPlayCell
        let item = lstSong[indexPath.row]
        cell.lblNumber.text = "\(indexPath.row + 1)"
        cell.item = item
        cell.fetchInfor()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        playSong(songName: self.lstSong[indexPath.row].path)
        //UIChange
        if indexPlaying == -1 {
            self.lstSong[indexPath.row].isPlay = true
            indexPlaying = indexPath.row
            tableView.reloadData()
        } else if indexPlaying != indexPath.row{
            self.lstSong[indexPath.row].isPlay = true
            self.lstSong[indexPlaying].isPlay = false
            tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0),IndexPath(row: indexPlaying, section: 0)], with: .none)
            indexPlaying = indexPath.row
        } else {
            print("Replay")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension ListSongPlayController : AudioPlayerDelegate {
    func audioPlayer(_ audioPlayer: AudioPlayer, didChangeStateFrom from: AudioPlayerState, to state: AudioPlayerState) {
        switch state {
        case .playing:
            btnPlay.setImage(UIImage(named: "ic_pause"), for: .normal)
            break
        case .paused:
            btnPlay.setImage(UIImage(named: "ic_big_play_album"), for: .normal)
            break
        case .stopped:
            btnPlay.setImage(UIImage(named: "ic_big_play_album"), for: .normal)
            break
        default:
            break
        }
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didUpdateProgressionTo time: TimeInterval, percentageRead: Float) {
        updateTimePlay(time: time, percent: percentageRead)
        if percentageRead == 100.0 {
            switch repeatState {
            case .nonRepeat:
                isShuffle ? playRandom() : playNextSong()
                break
            case .repeatOne:
                replaySong()
                break
            case .repeatAll:
                playNextSong()
                break
            }
        }
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didFindDuration duration: TimeInterval, for item: AudioItem) {
        setEndTime(time: duration)
        currentLength = Int(duration)
        updateScreen()
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didUpdateEmptyMetadataOn item: AudioItem, withData data: Metadata) {
        item.parseMetadata(data)
        if delegate != nil {
            delegate?.updateUI(item)
        }
    }
}
