//
//  RootSettingController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/11/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class RootSettingController: UINavigationController {

    class func create() -> RootSettingController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! RootSettingController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
