//
//  VideoPlayerController.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 9/9/20.
//  Copyright © 2020 nguyen hong doan. All rights reserved.
//

import UIKit

class VideoPlayerController: UIViewController {
    
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var currentNamePlay: UILabel!
    @IBOutlet weak var currentArtistPlay: UILabel!
    @IBOutlet weak var titleNextVideo: UILabel!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var tabView: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    
    var dataVideo = [VideoModel]()
    let nameVideoCell = "ItemVideoCell"
    var playerVideo: GPVideoPlayer?
    var isMute = false {
        didSet {
            if let player = self.playerVideo {
                player.isMuted = isMute
            }
        }
    }
    var indexStart = 0
    var currentIndexPlaying = 0 {
        didSet {
            if (self.dataVideo.count - 1) >= self.currentIndexPlaying {
                self.dataVideo[oldValue].isPlay = false
                let currentIndex = self.currentIndexPlaying + self.indexStart
                self.dataVideo[currentIndex].isPlay = true
                currentNamePlay.text = self.dataVideo[currentIndex].nameVideo
                currentArtistPlay.text = self.dataVideo[currentIndex].artist
                self.tabView.reloadData()
            }
        }
    }
    
    class func create() -> VideoPlayerController {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! VideoPlayerController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabView.delegate = self
        tabView.dataSource = self
        tabView.register(UINib.init(nibName: nameVideoCell, bundle: nil), forCellReuseIdentifier: nameVideoCell)
        configPlayer()
        self.loadData()
    }
    
    func configPlayer() {
        playerVideo = GPVideoPlayer.initialize(with: self.mediaView.bounds)
        playerVideo?.delegate = self
        if let player = self.playerVideo {
            player.isToShowPlaybackControls = true
            self.mediaView.addSubview(player)
        }
    }
    
    func loadData() {
        loadAllVideo()
        self.tabView.reloadData()
    }
    
    func loadAllVideo() {
        var lstUrl = [URL]()
        for item in dataVideo {
            if let musicPath = getMusicWithURL(item.path) {
                let mediumURL = URL(fileURLWithPath: musicPath)
                lstUrl.append(mediumURL)
            }
        }
        if let player = self.playerVideo {
            player.loadVideos(with: lstUrl)
            player.playVideo()
            self.currentIndexPlaying = 0
            self.dataVideo[self.currentIndexPlaying].isPlay = true
        }
    }
    
    func loadSubListVideo(_ indexPath: Int) {
        var lstUrl = [URL]()
        for (index, item) in dataVideo.enumerated() {
            if index >= indexPath, let musicPath = getMusicWithURL(item.path) {
                let mediumURL = URL(fileURLWithPath: musicPath)
                lstUrl.append(mediumURL)
            }
        }
        if let player = self.playerVideo {
            player.loadVideos(with: lstUrl)
            player.playVideo()
            self.dataVideo[(self.currentIndexPlaying + self.indexStart)].isPlay = true
        }
    }
    
    func resetStatusPlay() {
        for item in self.dataVideo {
            item.isPlay = false
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.playerVideo?.clearMediaPlayer()
        self.resetStatusPlay()
        self.dismiss(animated: true, completion: nil)
    }
}

extension VideoPlayerController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataVideo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nameVideoCell, for: indexPath) as! ItemVideoCell
        let dataForCell = self.dataVideo[indexPath.row]
        cell.nameVideo.text = dataForCell.nameVideo
        cell.artistName.text = dataForCell.artist
        cell.durationVideo.text = dataForCell.durantion
        cell.index = indexPath.row
        cell.btnShowOption.isHidden = true
        if dataForCell.artwork == nil {
            cell.imgVideo.isHidden = true
            cell.imgHidden.isHidden = false
        } else {
            cell.imgVideo.image = dataForCell.artwork
            cell.imgHidden.isHidden = true
            cell.imgVideo.isHidden = false
        }
        if dataForCell.isPlay {
            cell.indicatorView.isHidden = false
            cell.indicatorView.startAnimating()
        } else {
            cell.indicatorView.stopAnimating()
            cell.indicatorView.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.playVideoAtIndex(indexPath.row)
    }
}

extension VideoPlayerController {
    func playVideoAtIndex(_ index: Int) {
        if (self.dataVideo.count - 1) >= index {
            self.playerVideo?.clearMediaPlayer()
            self.resetStatusPlay()
            self.indexStart = index
            self.currentIndexPlaying = 0
            self.loadSubListVideo(index)
            self.tabView.reloadData()
        }
    }
}

extension VideoPlayerController: GPVideoPlayerDelegate {
    func currentItemIndexPlaying(_ indexPlaying: Int) {
        self.currentIndexPlaying = indexPlaying
    }
}
