//
//  OneDriveModel.swift
//  GomusicApp
//
//  Created by mac on 8/18/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class OneDriveModel: NSObject {
    var id = ""
    var cTag = ""
    var eTag = ""
    var childCount = 0
    var mimeType = ""
    var isFolder = false
    var createdDateTime = ""
    var lastModifiedDateTime = ""
    var name = ""
    var size: Int64 = 0
    var webUrl = ""
    var dowloadUrl = ""
    
    func initFromDic(_ dic: [String: Any]) {
        if let idValue = dic["id"] as? String {
            self.id = idValue
        }
        if let cTagValue = dic["cTag"] as? String {
            self.cTag = cTagValue
        }
        if let eTagValue = dic["eTag"] as? String {
            self.eTag = eTagValue
        }
        if let createdDateTimeValue = dic["createdDateTime"] as? String {
            self.createdDateTime = createdDateTimeValue
        }
        if let lastModifiedDateTimeValue = dic["lastModifiedDateTime"] as? String {
            self.lastModifiedDateTime = lastModifiedDateTimeValue
        }
        if let nameValue = dic["name"] as? String {
            self.name = nameValue
        }
        if let sizeValue = dic["size"] as? NSNumber {
            self.size = sizeValue.int64Value
        }
        if let webUrlValue = dic["webUrl"] as? String {
            self.webUrl = webUrlValue
        }
        if let dowloadUrlValue = dic["@microsoft.graph.downloadUrl"] as? String {
            self.dowloadUrl = dowloadUrlValue
        }
        if let fileValue = dic["file"] as? [String: Any] {
            self.isFolder = false
            if let mimetype = fileValue["mimeType"] as? String {
                self.mimeType = mimetype
            }
        }
        if let folderValue = dic["folder"] as? [String: Any] {
            self.isFolder = true
            if let childCountValue = folderValue["childCount"] as? NSNumber {
                self.childCount = childCountValue.intValue
            }
        }
    }
}

