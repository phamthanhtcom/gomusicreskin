//
//  MusicModel.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/18/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class MusicModel: NSObject {
    var id = -1
    var nameSong = ""
    var artist = ""
    var artwork : UIImage?
    var path = ""
    var isPlay = false
    override init() {
        super.init()
    }
    
    init(_ name : String, _ artist : String, _ artwork: UIImage?, path : String) {
        self.nameSong = name
        self.artist = artist
        self.artwork = artwork
        self.path = path
    }
}
