//
//  VideoModel.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 9/9/20.
//  Copyright © 2020 nguyen hong doan. All rights reserved.
//

import Foundation

class VideoModel: NSObject {
    var id = -1
    var nameVideo = ""
    var durantion = ""
    var artist = ""
    var artwork : UIImage?
    var path = ""
    var isPlay = false
    override init() {
        super.init()
    }
    
    init(_ name : String, _ artist : String, _ artwork: UIImage?, path : String, duration: String) {
        self.nameVideo = name
        self.artist = artist
        self.artwork = artwork
        self.path = path
        self.durantion = duration
    }
}
