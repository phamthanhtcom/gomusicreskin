//
//  AlbumModel.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/18/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class AlbumModel: NSObject, NSCoding {
    var name = ""
    var listSong = [String]()
    var isVideo = false
    
    init(name: String, listSong: [String], isVideo: Bool) {
        self.name = name
        self.listSong = listSong
        self.isVideo = isVideo
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let nameAl = aDecoder.decodeObject(forKey: "name") as! String
        let isVideo = aDecoder.decodeBool(forKey: "isVideo") 
        let listSong = aDecoder.decodeObject(forKey: "listSong") as! [String]
        self.init(name : nameAl, listSong : listSong, isVideo: isVideo)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(isVideo, forKey: "isVideo")
        aCoder.encode(listSong, forKey: "listSong")
    }
}
