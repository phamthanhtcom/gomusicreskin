//
//  ItemVideoCell.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 9/9/20.
//  Copyright © 2020 nguyen hong doan. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol ItemVideoCellDelegate {
    func showDetailVideo(_ index : Int)
}


class ItemVideoCell: UITableViewCell {
    
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var nameVideo: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var btnShowOption: UIButton!
    @IBOutlet weak var imgHidden: UIImageView!
    @IBOutlet weak var viewCheck: UIView!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var viewRound: GradientView!
    @IBOutlet weak var durationVideo: UILabel!
    @IBOutlet weak var indicatorView: NVActivityIndicatorView!
    
    var delegate : ItemVideoCellDelegate?
    var index = -1
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        self.imgVideo.layer.cornerRadius = 3
    }
    
    @IBAction func actionShowOption(_ sender: Any) {
        delegate?.showDetailVideo(index)
    }
    
}
