//
//  ItemSongCell.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/9/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol ItemSongCellDelegate {
    func showDetail(_ index : Int)
}

class ItemSongCell: UITableViewCell {
    
    @IBOutlet weak var imgSong: UIImageView!
    @IBOutlet weak var nameSong: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var btnShowOption: UIButton!
    @IBOutlet weak var imgHidden: UIImageView!
    @IBOutlet weak var viewCheck: UIView!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var viewRound: GradientView!
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    var delegate : ItemSongCellDelegate?
    var index = -1
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        self.imgSong.layer.cornerRadius = 3
        DispatchQueue.main.async {
            self.viewRound.layer.cornerRadius = self.viewRound.frame.height / 2
        }
    }
    
    @IBAction func actionShowOption(_ sender: Any) {
        delegate?.showDetail(index)
    }
}
