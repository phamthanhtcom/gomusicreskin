//
//  CustomItemAlbumCell.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/11/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class CustomItemAlbumCell: UITableViewCell {
    
    @IBOutlet weak var nameAlbum: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var btnPlayAlbum: UIButton!
    @IBOutlet weak var imgAlbum: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}
