//
//  FileCell.swift
//  GomusicApp
//
//  Created by mac on 8/13/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import SwiftyDropbox
import GoogleAPIClientForREST
class FileCell: UITableViewCell {
    @IBOutlet weak var imgFile: UIImageView!
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var imgDownload: UIImageView!
    
    var dropFile : Files.Metadata!
    var ggFile : GTLRDrive_File!
    var oneFile : OneDriveModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func fetchInforDrop(){
        lblFileName.text = dropFile.name
        
        if dropFile is Files.FolderMetadata {
            imgFile.image = UIImage(named: "ic_folder")
            imgDownload.isHidden = true
        } else if dropFile is Files.FileMetadata {
            if isMusicFile(dropFile) || isVideoFile(dropFile) {
                imgFile.image = UIImage(named: "ic_music")
                imgDownload.isHidden = false
            } else {
                imgFile.image = UIImage(named: "ic_file")
                imgDownload.isHidden = true
            }
        } else {
            imgFile.image = UIImage(named: "ic_file")
            imgDownload.isHidden = true
        }
    }
    
    func fetchInforGoogle(){
        lblFileName.text = ggFile.name
        if isFolder(ggFile) {
            imgFile.image = UIImage(named: "ic_folder")
            imgDownload.isHidden = true
        } else {
            if isMusicFile(ggFile) || isVideoFile(ggFile) {
                imgFile.image = UIImage(named: "ic_music")
                imgDownload.isHidden = false
            } else {
                imgFile.image = UIImage(named: "ic_file")
                imgDownload.isHidden = true
            }
        }
    }
    
    func fetchInforOneDrive(){
        lblFileName.text = oneFile.name
        if oneFile.isFolder {
            imgFile.image = UIImage(named: "ic_folder")
            imgDownload.isHidden = true
        } else {
            if isMusicFile(oneFile) || isVideoFile(oneFile) {
                imgFile.image = UIImage(named: "ic_music")
                imgDownload.isHidden = false
            } else {
                imgFile.image = UIImage(named: "ic_file")
                imgDownload.isHidden = true
            }
        }
    }
    
}
