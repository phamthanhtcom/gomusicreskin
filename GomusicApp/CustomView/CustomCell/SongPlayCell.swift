//
//  SongPlayCell.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/16/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class SongPlayCell: UITableViewCell {
    @IBOutlet weak var indicatorView: NVActivityIndicatorView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var nameSong: UILabel!
    @IBOutlet weak var artistName: UILabel!
    
    var item : MusicModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func fetchInfor(){
        nameSong.text = item.nameSong
        artistName.text = item.artist
        if item.isPlay {
            lblNumber.isHidden = true
            indicatorView.isHidden = false
            indicatorView.startAnimating()
        } else {
            lblNumber.isHidden = false
            indicatorView.stopAnimating()
            indicatorView.isHidden = true
        }
    }
    
}
