//
//  SongItemCell.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 9/6/20.
//  Copyright © 2020 nguyen hong doan. All rights reserved.
//

import UIKit
import FSPagerView

class SongItemCell: FSPagerViewCell {
    
    @IBOutlet weak var imgSong: UIImageView!
    @IBOutlet weak var nameSong: UILabel!
    @IBOutlet weak var artistName: UILabel!
    
    var item : MusicModel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.shadowColor = UIColor.clear.cgColor
    }
    
    func fetchInfor(){
        nameSong.text = item.nameSong
        artistName.text = item.artist
        if item.artwork == nil {
            imgSong.isHidden = true
        } else {
            imgSong.image = item.artwork
            imgSong.isHidden = false
        }
    }
    
}
