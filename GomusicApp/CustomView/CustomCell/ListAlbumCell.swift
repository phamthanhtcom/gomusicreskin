//
//  ListAlbumCell.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/9/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
protocol ListAlbumCellDelegate : class {
    func onClickItem(_ index : Int, _ isEmptyList : Bool)
}
class ListAlbumCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    let cellItemAlbum = "ItemAlbumCell"
    var listAlbum = [AlbumModel]()
    var delegate : ListAlbumCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: cellItemAlbum, bundle: nil), forCellWithReuseIdentifier: cellItemAlbum)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        selectionStyle = .none
    }
    
    func loadData(_ list : [AlbumModel]) {
        listAlbum = list
        collectionView.reloadData()
    }
}
extension ListAlbumCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if listAlbum.isEmpty {
            return 1
        }
        return listAlbum.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellItemAlbum, for: indexPath) as! ItemAlbumCell
        if listAlbum.isEmpty {
            cell.subTitle.text = ""
            cell.imgAlbum.image = UIImage(named: "plus")?.tint(with: .white)
            cell.nameAlbum.text = "New Album"
        } else {
            cell.imgAlbum.image = listAlbum[indexPath.item].isVideo ? UIImage(named: "multimedia") : UIImage(named: "music-player")
            cell.nameAlbum.text = listAlbum[indexPath.row].name
            cell.subTitle.text = String.init(format: listAlbum[indexPath.item].isVideo ? LocalizedString("count_videos", comment: "") : LocalizedString("count_songs", comment: ""), listAlbum[indexPath.item].listSong.count)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.height - 47) * (190 / 125) - 10
        return CGSize(width: width, height: collectionView.bounds.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if listAlbum.isEmpty {
            delegate?.onClickItem(indexPath.item, true)
        } else {
            delegate?.onClickItem(indexPath.item, false)
        }
    }
}
