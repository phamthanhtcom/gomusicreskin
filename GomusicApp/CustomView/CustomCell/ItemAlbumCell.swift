//
//  ItemAlbumCell.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/9/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit

class ItemAlbumCell: UICollectionViewCell {
    @IBOutlet weak var imgAlbum: UIImageView!
    @IBOutlet weak var nameAlbum: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
