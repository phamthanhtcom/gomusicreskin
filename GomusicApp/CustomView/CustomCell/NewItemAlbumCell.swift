//
//  NewItemAlbumCell.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 9/13/20.
//  Copyright © 2020 nguyen hong doan. All rights reserved.
//

import UIKit

class NewItemAlbumCell: UICollectionViewCell {
    
    @IBOutlet weak var imgAlbum: UIImageView!
    @IBOutlet weak var nameAlbum: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
