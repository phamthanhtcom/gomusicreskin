//
//  HeaderLibaryCell.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/9/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
enum TYPELIST {
    case ALBUM
    case SONGS
    case VIDEO
}
protocol HeaderDelegate : class {
    func showMenu(_ type : TYPELIST)
    func add(_ type: TYPELIST)
}
class HeaderLibraryCell: UITableViewHeaderFooterView {
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var nameHeader: UILabel!
    weak var delegate : HeaderDelegate?
    var typeList : TYPELIST = .ALBUM
    
    @IBAction func actionShow(_ sender: Any) {
        delegate?.showMenu(typeList)
    }
    @IBAction func actionAdd(_ sender: Any) {
        delegate?.add(typeList)
    }
    
}
