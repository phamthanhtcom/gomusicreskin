//
//  CustomImportItem.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/2/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
protocol SignOutDelegate {
    func signOutWithType(_ type : String)
}
@IBDesignable
class CustomImportItem: UIButton {
    
    @IBOutlet weak var lbImport: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var imgImport: UIImageView!
    @IBOutlet weak var btnSignOut: UIButton!
    
    @objc open var handlerLogin: ((CustomImportItem) -> Void)? = nil
    var delegate : SignOutDelegate?
    let nibName = "CustomImportItem"
    var view : UIView!
    
    @IBInspectable var image: UIImage? = nil {
        didSet {
            imgImport.image = image
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.count == 1 {
            let touch = touches.first
            if touch?.tapCount == 1 {
                if touch?.location(in: self) == nil { return }
                handlerLogin?(self)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetUp()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetUp()
    }
    
    func xibSetUp() {
        view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() ->UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    @IBAction func signOutTapped(_ sender: Any) {
        if delegate != nil {
            delegate?.signOutWithType(self.lbImport.text!)
        }
    }
}
