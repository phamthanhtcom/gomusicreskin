//
//  CustomAlertDetailSong.swift
//  GomusicApp
//
//  Created by doanbk on 8/20/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
enum TypeShow {
    case ALBUM
    case SONG
    case VIDEO
}
class CustomAlertDetailSong: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    var delegate: CustomAlertDetailSongDelegate?
    var indexItem = -1
    var infoSong : MusicModel?
    var infoVideo : VideoModel?
    var infoAlbum : AlbumModel?
    var type : TypeShow = .SONG
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    @IBOutlet weak var viewHidden: UIView!
    @IBOutlet weak var heightAlert: NSLayoutConstraint!
    @IBOutlet weak var lbRename: UILabel!
    @IBOutlet weak var viewRename: UIView!
    
    class func create() -> CustomAlertView {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self)) as! CustomAlertView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch type {
        case .SONG:
            self.titleLabel.text = infoSong?.nameSong
            self.messageLabel.text = infoSong?.artist
            heightAlert.constant = 170
            lbRename.text = "Edit Title & Artist"
            viewRename.isHidden = true
        case .VIDEO:
            self.titleLabel.text = infoVideo?.nameVideo
            self.messageLabel.text = infoVideo?.artist
            heightAlert.constant = 170
            lbRename.text = "Edit Title & Artist"
            viewRename.isHidden = true
        default:
            self.titleLabel.text = infoAlbum?.name
            if let infoAlbum = infoAlbum {
                self.messageLabel.text = String.init(format: infoAlbum.isVideo ? LocalizedString("count_videos", comment: "") : LocalizedString("count_songs", comment: ""), infoAlbum.listSong.count)
            } else {
                self.messageLabel.text = ""
            }
            heightAlert.constant = 170
            lbRename.text = "Rename"
            viewHidden.isHidden = true
            viewRename.isHidden = false
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
    }
    
    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    @IBAction func onAddToAlbum(_ sender: Any) {
        delegate?.addToAlbumTapped(controler: self, indexItem, type)
    }
    
    @IBAction func onEditName(_ sender: Any) {
        delegate?.editNameTapped(controler: self, indexItem, type)
    }
    @IBAction func onDelete(_ sender: Any) {
        delegate?.deleteTapped(controler: self, indexItem, type)
    }
    @IBAction func onDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
