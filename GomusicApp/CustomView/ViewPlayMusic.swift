import Foundation
import UIKit
import FSPagerView
import MediaPlayer

enum RepeatState: Int {
    case repeatOne = 1
    case repeatAll = 2
    case nonRepeat = 0
}
protocol UpdateSong {
    func updateUI(_ item : AudioItem)
}

class ViewPlayMusic: UIView {
    
    @IBOutlet weak var viewMiniBar: UIView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var widthViewContent: NSLayoutConstraint!
    @IBOutlet weak var widthMinibar: NSLayoutConstraint!
    @IBOutlet weak var imgMiniBar: UIImageView!
    @IBOutlet weak var viewCornerButtonPlay: UIView!
    
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var lbDuration: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var btnRandom: UIButton!
    @IBOutlet weak var btnPre: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnRepeat: UIButton!
    @IBOutlet weak var btnMute: UIButton!
    
    @IBOutlet weak var currentArtistName: UILabel!
    @IBOutlet weak var currentSongName: UILabel!
    @IBOutlet weak var btnPlayMini: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnFull: UIButton!
    
    @IBOutlet weak var btnListMusic: UIButton!
    var isPause = false
    let cellSongName = "SongItemCell"
    var lstSong = [MusicModel]()
    var statusMuted = false {
        didSet {
            player.volume = statusMuted ? 0 : 1
            statusMuted ? btnMute.setImage(UIImage(named: "ic_mute"), for: .normal) : btnMute.setImage(UIImage(named: "ic_volumn"), for: .normal)
        }
    }
    var indexPlaying = -1 {
        didSet {
            if self.lstSong.count > indexPlaying {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.pagerView.scrollToItem(at: self.indexPlaying, animated: true)
                }
            }
        }
    }
    let player = AudioPlayer()
    var currentLength = 0
    var repeatState : RepeatState = .nonRepeat
    var isShuffle = false
    var delegate : UpdateSong?
    var currentAudio : AudioItem?
    var nowPlayingInfor = [String : Any]()
    
    private let spacingViewVideo:CGFloat = 6 // leading trailing when collapse
    private let maxPlayerHeight: CGFloat = 250
    private let miniPlayerBeginCollapse: CGFloat = 100
    private let miniPlayerWidthCollapse: CGFloat = 100
    private let miniPlayerHeight: CGFloat = 100
    private let heightMenu: CGFloat = Device.hasTopNotch ? (65 + 34) : 65
    
    class func createViewFromNib() -> ViewPlayMusic? {
        let bundle = Bundle(for: ViewPlayMusic.self)
        let nib = UINib(nibName: "ViewPlayMusic", bundle: bundle)
        return nib.instantiate(withOwner: ViewPlayMusic.self, options: nil).first as? ViewPlayMusic
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let panGes = UIPanGestureRecognizer(target: self, action: #selector(drag(_:)))
        self.addGestureRecognizer(panGes)
        DispatchQueue.main.async {
            self.imgMiniBar.layer.cornerRadius = self.imgMiniBar.frame.width / 2
            self.viewCornerButtonPlay.layer.cornerRadius = 5
            self.frame.size.height = UIScreen.main.bounds.height
            self.viewMiniBar.roundCorners([.topLeft, .topRight], radius: 15)
        }
        settingTableView()
        getAllSong()
        player.delegate = self
        slider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.widthMinibar.constant = UIScreen.main.bounds.width
        self.widthViewContent.constant = UIScreen.main.bounds.width
    }
    
    @objc func drag(_ gesture:UIPanGestureRecognizer) {
        let translation = gesture.translation(in: self)
        let contentHeight = UIScreen.main.bounds.height - miniPlayerHeight - heightMenu
        switch gesture.state {
        case .began:
            break
        case .changed:
            let containerY = frame.origin.y
            if containerY <= contentHeight && containerY >= bounds.origin.y {
                if containerY + translation.y < 0 {
                    frame.origin.y = bounds.origin.y
                } else if containerY + translation.y > contentHeight {
                    frame.origin.y = contentHeight
                } else {
                    frame.origin.y += translation.y
                }
                gesture.setTranslation(.zero, in: self)
            }
            let scale = frame.origin.y / contentHeight
            viewContent.alpha = 1 - scale
            //
            frame.size.height = UIScreen.main.bounds.height - frame.origin.y
            //
            //            let heightCollapseH = UIScreen.main.bounds.height - frame.origin.y - heightMenu
            //            if heightCollapseH < miniPlayerBeginCollapse {
            //                let a = (heightCollapseH - miniPlayerHeight)
            //                let b = (miniPlayerBeginCollapse - miniPlayerHeight)
            //                let scaleW = a/b
            //                viewMiniBar.alpha = 1 - scaleW
            //            } else {
            //                viewMiniBar.alpha = 0
        //            }
        case .ended:
            let velocity = gesture.velocity(in: self)
            if velocity.y > 0 {
                UIView.animate(withDuration: 0.2, animations: {
                    self.frame.origin.y = contentHeight
                    self.frame.size.height = self.miniPlayerHeight
                    self.viewMiniBar.isHidden = false
                    self.viewContent.alpha = 0
                    self.viewMiniBar.alpha = 1
                    self.layoutIfNeeded()
                }) { _ in
                }
            } else {
                self.setPlayerFrame(frame: CGSize(width: UIScreen.main.bounds.width, height: self.maxPlayerHeight))
                UIView.animate(withDuration: 0.2, animations: {
                    self.frame.origin.y = 0
                    self.frame.size.height = UIScreen.main.bounds.height
                    self.viewMiniBar.isHidden = true
                    self.viewContent.alpha = 1
                    self.viewMiniBar.alpha = 0
                    self.layoutIfNeeded()
                }) { _ in
                }
            }
            break
        default:
            return
        }
    }
    
    fileprivate func settingTableView() {
        pagerView.delegate = self
        pagerView.dataSource = self
        self.pagerView.register(UINib.init(nibName: cellSongName, bundle: nil), forCellWithReuseIdentifier: cellSongName)
        self.pagerView.itemSize = CGSize.init(width: self.pagerView.frame.size.width * 0.6, height: self.pagerView.frame.size.height * 0.85)
        self.pagerView.decelerationDistance = FSPagerView.automaticDistance
        self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
        NotificationCenter.default.addObserver(self, selector: #selector(getAllSong), name: NSNotification.Name(rawValue: Constants.RELOAD_LIST_SONG), object: nil)
    }
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event != nil{
            switch event!.subtype {
            case .remoteControlPlay, .remoteControlPause:
                pauseAndPlay()
                break
            case .remoteControlPreviousTrack:
                playPreviousSong()
                break
            case .remoteControlNextTrack:
                playNextSong()
                break
            default:
                break
            }
        }
    }
    
    func playAlbum(lstSong : [MusicModel], indexPlay : Int, isRandom : Bool){
        if lstSong.count != 0 {
            self.lstSong = lstSong
            playSong(songName: self.lstSong[indexPlay].path)
            setDataToMiniBar(indexPlay)
            for item in self.lstSong {
                item.isPlay = false
            }
            self.lstSong[indexPlay].isPlay = true
            pagerView.reloadData()
            indexPlaying = indexPlay
        }
        if isRandom {
            self.isShuffle = isRandom
            let colorTint = isShuffle ? color(0xEB3B5A) : color(0xBDBDBD)
            btnRandom.tintColor = colorTint
        }
    }
    
    @objc func getAllSong(){
        self.lstSong = getAllFileInFolder()
        pagerView.reloadData()
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent){
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                if !player.state.isStopped {
                    if !player.state.isPaused {
                        player.pause()
                    } else {
                        isPause = true
                    }
                }
                break
            case .moved:
                break
            case .ended:
                if currentLength == 0 {
                    slider.value = 0
                } else {
                    if player.state.isPaused {
                        let timeSeek = Int(slider.value * Float(currentLength))
                        player.seek(to: TimeInterval(timeSeek))
                        if !isPause {
                            player.resume()
                        } else {
                            isPause = false
                        }
                    } else if player.state.isStopped {
                        slider.value = 100
                    }
                }
                break
            default:
                break
            }
        }
    }
    
    @IBAction func showFull(_ sender: Any) {
        self.setFullPlayView()
    }
    
    @IBAction func actionShowListPlay(_ sender: Any) {
        let lstPlaying = ListSongPlayingController.create()
        lstPlaying.listSong = self.lstSong
        lstPlaying.delegate = self
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            if let tabbar = delegate.tabbar {
                tabbar.present(lstPlaying, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func muteMusic(_ sender: Any) {
        statusMuted.toggle()
    }
    
    @IBAction func shuffleTapped(_ sender: Any) {
        isShuffle = !isShuffle
//        let image = isShuffle ? UIImage(named: "ic_asyns_color") : UIImage(named: "ic_asyns")
        let colorTint = isShuffle ? color(0xEB3B5A) : color(0xBDBDBD)
//        btnRandom.setImage(image, for: .normal)
        btnRandom.tintColor = colorTint
    }
    
    @IBAction func previousTapped(_ sender: Any) {
        playPreviousSong()
    }
    @IBAction func playAndPauseTapped(_ sender: Any) {
        pauseAndPlay()
    }
    @IBAction func nextTapped(_ sender: Any) {
        playNextSong()
    }
    @IBAction func repeatTapped(_ sender: Any) {
        var current = repeatState.rawValue
//        if current == 2 {
//            current = 0
//        } else {
//            current += 1
//        }
        if current == 2 {
            current = 0
        } else {
            current = 2
        }
        repeatState = RepeatState.init(rawValue: current)!
        switch repeatState {
        case .nonRepeat:
            btnRepeat.imageView?.tintColor = color(0xBDBDBD)
//            btnRepeat.setImage(UIImage(named: "ic_repeat"), for: .normal)
            break
        case .repeatAll:
            btnRepeat.imageView?.tintColor = color(0xEB3B5A)
//            btnRepeat.setImage(UIImage(named: "ic_repeat_color"), for: .normal)
            break
        case .repeatOne:
//            btnRepeat.setImage(UIImage(named: "ic_repeat_one"), for: .normal)
            break
        }
    }
    
    // Music Method
    func playSong(songName : String) {
        if let musicPath = getMusicWithURL(songName){
            let mediumURL = URL(fileURLWithPath: musicPath)
            if let item = AudioItem(highQualitySoundURL: nil, mediumQualitySoundURL: mediumURL, lowQualitySoundURL: nil){
                player.play(item: item)
                currentAudio = item
            }
        }
    }
    
    func setEndTime(time: Double) {
        let duration = getTimeString(seconds: Int(time))
        lbDuration.text = duration
    }
    
    func updateTimePlay(time: Double, percent: Float) {
        let duration = getTimeString(seconds: Int(time))
        
        self.startTime.text = duration
        self.slider.value = percent/100.0
    }
    
    func seekMusicToPos(pos: Int){
        if (self.player.state == .playing) {
            self.player.seek(to: TimeInterval(pos))
        }
    }
    
    func pauseAndPlay(){
        switch player.state {
        case .playing:
            player.pause()
            break
        case .paused:
            player.resume()
            break
        case .stopped:
            player.seek(to: 0)
            player.resume()
            break
        default:
            break
        }
    }
    
    func playNextSong(){
        if indexPlaying != -1 {
            self.lstSong[indexPlaying].isPlay = false
            if indexPlaying < self.lstSong.count - 1{
                let temp = indexPlaying + 1
                self.lstSong[temp].isPlay = true
//                tableView.reloadRows(at: [IndexPath(row: indexPlaying, section: 0),IndexPath(row: temp, section: 0)], with: .none)
                playSong(songName: self.lstSong[temp].path)
                setDataToMiniBar(temp)
                indexPlaying = temp
            } else {
                self.lstSong[0].isPlay = true
//                tableView.reloadRows(at: [IndexPath(row: indexPlaying, section: 0),IndexPath(row: 0, section: 0)], with: .none)
                playSong(songName: self.lstSong[0].path)
                setDataToMiniBar(0)
                indexPlaying = 0
            }
        }
    }
    
    func playPreviousSong(){
        if indexPlaying != -1 {
            self.lstSong[indexPlaying].isPlay = false
            if indexPlaying > 0 {
                let temp = indexPlaying - 1
                self.lstSong[temp].isPlay = true
//                tableView.reloadRows(at: [IndexPath(row: indexPlaying, section: 0),IndexPath(row: temp, section: 0)], with: .none)
                playSong(songName: self.lstSong[temp].path)
                setDataToMiniBar(temp)
                indexPlaying = temp
            } else {
                let temp = self.lstSong.count - 1
                self.lstSong[temp].isPlay = true
//                tableView.reloadRows(at: [IndexPath(row: 0, section: 0),IndexPath(row: temp, section: 0)], with: .none)
                playSong(songName: self.lstSong[temp].path)
                setDataToMiniBar(temp)
                indexPlaying = temp
            }
        }
    }
    
    func setDataToMiniBar(_ index: Int) {
        if self.lstSong.count > index {
            let data = self.lstSong[index]
            if data.artwork == nil {
                imgMiniBar.isHidden = true
            } else {
                imgMiniBar.image = data.artwork
                imgMiniBar.isHidden = false
            }
            self.currentSongName.text = data.nameSong
            self.currentArtistName.text = data.artist
        }
    }
    
    func playRandom(){
        var randomNumber : Int
        repeat {
            randomNumber = Int(arc4random_uniform(UInt32(self.lstSong.count - 1)))
        } while indexPlaying == randomNumber
        self.lstSong[indexPlaying].isPlay = false
        self.lstSong[randomNumber].isPlay = true
        playSong(songName: self.lstSong[randomNumber].path)
        setDataToMiniBar(randomNumber)
        indexPlaying = randomNumber
    }
    
    func replaySong(){
        playSong(songName: self.lstSong[indexPlaying].path)
        setDataToMiniBar(indexPlaying)
    }
    
    func updateScreen(){
        if currentAudio != nil {
            nowPlayingInfor[MPMediaItemPropertyTitle] = currentAudio!.title
            nowPlayingInfor[MPMediaItemPropertyArtist] = currentAudio!.artist
            if let image = currentAudio!.artwork {
                nowPlayingInfor[MPMediaItemPropertyArtwork] = image
            }
            
            // Set the metadata
            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfor
        }
    }
    
    func setFullPlayView() {
        UIView.animate(withDuration: 0.2, animations: {
            self.frame.origin.y = 0
            self.frame.size.height = UIScreen.main.bounds.height
            self.viewMiniBar.isHidden = true
            self.viewContent.alpha = 1
            self.viewMiniBar.alpha = 0
            self.layoutIfNeeded()
        }) { _ in
        }
    }
    
    func setMiniPlayView() {
        let contentHeight = UIScreen.main.bounds.height - miniPlayerHeight - heightMenu
        UIView.animate(withDuration: 0.2, animations: {
            self.frame.origin.y = contentHeight
            self.frame.size.height = self.miniPlayerHeight
            self.viewMiniBar.isHidden = false
            self.viewContent.alpha = 0
            self.viewMiniBar.alpha = 1
            self.layoutIfNeeded()
        }) { _ in
        }
    }
    @IBAction func showMiniBar(_ sender: Any) {
        setMiniPlayView()
    }
    
    func setPlayerFrame(frame: CGSize, withAnimation: Bool = false) {
        //        MusicManager.shared().currentLayer.frame.size = frame
        if !withAnimation {
            //            MusicManager.shared().currentLayer.removeAllAnimations()
        }
    }
    
    deinit {
        print("ViewPlayVideo deinit")
    }
    
    @IBAction func actionPlay(_ sender: Any) {
        pauseAndPlay()
    }
    @IBAction func actionClose(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.frame.origin.y = UIScreen.main.bounds.height
            self.alpha = 0
        }) { _ in
            if let app = UIApplication.shared.delegate as? AppDelegate {
                self.player.stop()
                app.removeViewPlay()
            }
        }
    }
}

extension ViewPlayMusic: FSPagerViewDelegate, FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return lstSong.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: cellSongName, at: index) as! SongItemCell
        let item = lstSong[index]
        cell.item = item
        cell.fetchInfor()
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        playSong(songName: self.lstSong[index].path)
        setDataToMiniBar(index)
        //UIChange
        if indexPlaying == -1 {
            self.lstSong[index].isPlay = true
            indexPlaying = index
            self.pagerView.reloadData()
        } else if indexPlaying != index{
            self.lstSong[index].isPlay = true
            self.lstSong[indexPlaying].isPlay = false
            indexPlaying = index
        } else {
            print("Replay")
        }
    }
}

extension ViewPlayMusic : AudioPlayerDelegate {
    func audioPlayer(_ audioPlayer: AudioPlayer, didChangeStateFrom from: AudioPlayerState, to state: AudioPlayerState) {
        switch state {
        case .playing:
            btnPlay.setImage(UIImage(named: "ic_pause"), for: .normal)
            btnPlayMini.setImage(UIImage.init(named: "ic_btn_pause_mini"), for: .normal)
            break
        case .paused:
            btnPlay.setImage(UIImage(named: "ic_big_play_album"), for: .normal)
            btnPlayMini.setImage(UIImage.init(named: "ic_btn_play_mini"), for: .normal)
            break
        case .stopped:
            btnPlay.setImage(UIImage(named: "ic_big_play_album"), for: .normal)
            btnPlayMini.setImage(UIImage.init(named: "ic_btn_play_mini"), for: .normal)
            break
        default:
            break
        }
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didUpdateProgressionTo time: TimeInterval, percentageRead: Float) {
        updateTimePlay(time: time, percent: percentageRead)
        if percentageRead == 100.0 {
            switch repeatState {
            case .nonRepeat:
                isShuffle ? playRandom() : playNextSong()
                break
            case .repeatOne:
                replaySong()
                break
            case .repeatAll:
                playNextSong()
                break
            }
        }
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didFindDuration duration: TimeInterval, for item: AudioItem) {
        setEndTime(time: duration)
        currentLength = Int(duration)
        updateScreen()
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didUpdateEmptyMetadataOn item: AudioItem, withData data: Metadata) {
        item.parseMetadata(data)
        if delegate != nil {
            delegate?.updateUI(item)
        }
    }
}

extension ViewPlayMusic: ListSongPlayingDelegate {
    func onClickItem(_ control: UIViewController, _ index: Int, _ itemData: MusicModel) {
        control.dismiss(animated: true) {
            self.playSong(songName: self.lstSong[index].path)
            self.setDataToMiniBar(index)
            //UIChange
            if self.indexPlaying == -1 {
                self.lstSong[index].isPlay = true
                self.indexPlaying = index
                self.pagerView.reloadData()
            } else if self.indexPlaying != index{
                self.lstSong[index].isPlay = true
                self.lstSong[self.indexPlaying].isPlay = false
                self.indexPlaying = index
            } else {
                print("Replay")
            }
        }
    }
}
