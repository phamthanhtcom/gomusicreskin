//
//  ViewOption.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 9/12/20.
//  Copyright © 2020 nguyen hong doan. All rights reserved.
//

import UIKit

protocol ViewOptionDelegate {
    func addOption(_ type: TYPELIST)
    func deleteOption(_ type: TYPELIST)
}

class ViewOption: UIView {

    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnRemove: UIButton!
    
    let nibName = "ViewOption"
    var view : UIView!
    var typeList: TYPELIST = .SONGS
    var delegate: ViewOptionDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetUp()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetUp()
    }
    
    func xibSetUp() {
        view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() ->UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    @IBAction func actionAdd(_ sender: Any) {
        delegate?.addOption(self.typeList)
    }
    
    @IBAction func actionDelete(_ sender: Any) {
        delegate?.deleteOption(self.typeList)
    }
}
