//
//  CustomAlertViewDelegate.swift
//  CustomAlertView
//
//  Created by Daniel Luque Quintana on 16/3/17.
//  Copyright © 2017 dluque. All rights reserved.
//

protocol CustomAlertViewDelegate: class {
    func okButtonTapped(textFieldValue: String, typeVideo: Bool)
    func cancelButtonTapped()
}

protocol CustomAlertDetailSongDelegate: class {
    func addToAlbumTapped(controler: UIViewController,_ index : Int, _ type: TypeShow)
    func editNameTapped(controler: UIViewController, _ index : Int, _ type : TypeShow)
    func deleteTapped(controler: UIViewController, _ index : Int, _ type : TypeShow)
}

