//
//  CustomButtonSetting.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/11/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
@IBDesignable
class CustomButtonSetting: UIButton {

    @IBOutlet weak var lbSetting: UILabel!
    @IBOutlet weak var imgSetting: UIImageView!
    let nibName = "CustomButtonSetting"
    var view : UIView!
    @objc open var handlerSetting: ((CustomButtonSetting) -> Void)? = nil
    @IBInspectable var image: UIImage? = nil {
        didSet {
            imgSetting.image = image
        }
    }
    
    @IBInspectable var titleSetting: String = "" {
        didSet {
            lbSetting.text = titleSetting
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetUp()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetUp()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.count == 1 {
            let touch = touches.first
            if touch?.tapCount == 1 {
                if touch?.location(in: self) == nil { return }
                handlerSetting?(self)
            }
        }
    }
    
    func xibSetUp() {
        view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() ->UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
