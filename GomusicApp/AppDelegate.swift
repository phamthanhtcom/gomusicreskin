//
//  AppDelegate.swift
//  GomusicApp
//
//  Created by nguyen hong doan on 8/2/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import UIKit
import SwiftyDropbox
import GoogleSignIn
import SVProgressHUD
import MSAL
import Material
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tabbar : TabarController?
    var viewPlay: ViewPlayMusic?
    var main: MainNavigationDrawerController?
    var isShowBarMini = false
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        DropboxClientsManager.setupWithAppKey(Constants.DROPBOX_APPKEY)
        GIDSignIn.sharedInstance().clientID = Constants.CLIENT_ID_DRIVER
        window = UIWindow(frame: UIScreen.main.bounds)
        main = MainNavigationDrawerController(rootViewController: TabarController.create(), leftViewController: nil, rightViewController: SettingController.create())
        main?.delegate = self
        window!.rootViewController = main
        window!.makeKeyAndVisible()
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.black)
        createRootFolder()
        UserDefaults.standard.set(0, forKey: Constants.NUMBER_NOTIFY)
        application.beginReceivingRemoteControlEvents()
        return true
    }
    
    func addViewPlay() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SHOW_VIEW_PLAY"), object: nil, userInfo: nil)
        isShowBarMini = true
        if let viewPlay = self.viewPlay {
            viewPlay.setFullPlayView()
            return
        }
        
        if let view = ViewPlayMusic.createViewFromNib() {
            viewPlay = view
            window?.addSubview(view)
        }
    }
    
    func hideViewPlay(_ isHidden: Bool) {
        if let viewPlay = self.viewPlay {
            viewPlay.isHidden = isHidden
        }
    }
    
    func removeViewPlay() {
        isShowBarMini = false
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HIDE_VIEW_PLAY"), object: nil, userInfo: nil)
        viewPlay?.removeFromSuperview()
        viewPlay = nil
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        GIDSignIn.sharedInstance()?.handle(url)
        MSALPublicClientApplication.handleMSALResponse(url, sourceApplication: sourceApplication ?? "")
        return true 
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if (url.absoluteString.contains("google")) {
            return GIDSignIn.sharedInstance().handle(url)
        } else {
            DropboxClientsManager.handleRedirectURL(url) { (authResult) in
                var success = false
                let mesg = ""
                switch authResult {
                case .success:
                    print("Success! User is logged into Dropbox.")
                    NotificationCenter.default.post(name: Notification.Name("DROPBOX_AUTHEN_SILENT"), object: nil, userInfo: nil)
                    success = true
                case .cancel:
                    print("Authorization flow was manually canceled by user!")
                case .error(_, let description):
                    print("Error: \(String(describing: description))")
                case .none:
                    print("Case None")
                }
                NotificationCenter.default.post(name: Notification.Name("DROPBOX_AUTHEN"), object: nil, userInfo: ["success": success, "message": mesg])
            }
        }
        return true
    }
}
extension AppDelegate: NavigationDrawerControllerDelegate {
    func navigationDrawerController(navigationDrawerController: NavigationDrawerController, didClose position: NavigationDrawerPosition) {
        self.hideViewPlay(false)
    }
    
    func navigationDrawerController(navigationDrawerController: NavigationDrawerController, willOpen position: NavigationDrawerPosition) {
        self.hideViewPlay(true)
    }
}

