//
//  Constant.swift
//  GomusicApp
//
//  Created by mac on 8/7/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import Foundation
class Constants {
    static let DROPBOX_APPKEY = "nso3qozdrzwr36u"
    static let CLIENT_ID_DRIVER = "264095654457-idocqu24kkuiakkv8n9i6c6h2npf844s.apps.googleusercontent.com"
    //Config MSAL ONEDRIVE
    static let CLIENT_ID_ONEDRIVE = "b93cb0f7-1d46-4611-a3fd-a570420811d1"
    static let kRedirectUri = "msauth.com.vupham.cloud.music://auth"
    //END CONFIG MSAL
    static let DROP_BOX_VC = "DROP_BOX_VC"
    static let GG_DRIVE_VC = "GG_DRIVE_VC"
    static let ONE_DRIVER_VC = "ONE_DRIVER_VC"
    static let MUSIC_FILE_EXT = ["aac", "aif", "aiff", "flac", "m4a", "mp3", "wav", "caf"]
    static let VIDEO_FILE_EXT = ["mp4","mov","ogx","ogv","oga","ogg","f4b","m4r","m4b","f4a","f4v","m4v"]
    static let ROOT = "RootMusic"
    static let UPDATE_LIST_ALBUM = "UPDATE_LIST_ALBUM"
    //OneDrive link API
//    static let kAuthority = "https://login.microsoftonline.com/common"
    static let kAuthority = "https://login.microsoftonline.com/consumers"
    static let kGraphURI = "https://graph.microsoft.com/v1.0/me/drive/%@/children"
    //Mimetype
    static let GG_MIME_FOLDER = "application/vnd.google-apps.folder"
    //list album
    static let LIST_ALBUM = "LIST_ALBUM"
    static let NUMBER_NOTIFY = "NUMBER_NOTIFY"
    
    //Key
    static let RELOAD_LIST_SONG = "RELOAD_LIST_SONG"
    //Link and Email
    static let YOUR_APP_STORE_URL = "https://www.google.com/?client=safari"
    static let EMAIL_FEEDBACK = "emailFeedBack@gmail.com"
    static let APP_MORE_STORE_URL = "https://apps.apple.com/vn/developer/truong-hoang/id1035202136"
    
    static let MAX_VIDEO_SIZE = 300.0
}

struct Device {
    // iDevice detection code
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA = UIScreen.main.scale >= 2.0
    static let SCREEN_WIDTH = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH < 568
    static let IS_IPHONE_5 = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6 = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X_OR_XS = IS_IPHONE && SCREEN_MAX_LENGTH == 812
    static let IS_IPHONE_XR_OR_XS_MAX = IS_IPHONE && SCREEN_MAX_LENGTH == 896
    static var DEVICEID: String {
        if let vender = UIDevice.current.identifierForVendor {
            return vender.uuidString
        }
        return ""
    }
    
    static var hasTopNotch: Bool {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows.filter { $0.isKeyWindow }.first?.safeAreaInsets.top ?? 0 > 20
        } else {
            if #available(iOS 11.0, *) {
                return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
            } else {
                return false
            }
        }
    }
}
