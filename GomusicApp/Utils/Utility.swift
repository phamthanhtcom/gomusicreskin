//
//  Utility.swift
//  GomusicApp
//
//  Created by mac on 8/15/19.
//  Copyright © 2019 nguyen hong doan. All rights reserved.
//

import Foundation
import GoogleAPIClientForREST
import SwiftyDropbox
import MediaPlayer

func isFolder(_ file : GTLRDrive_File) -> Bool{
    if (file.fileExtension == nil && file.mimeType == Constants.GG_MIME_FOLDER) {
        return true
    }
    return false
}

func isMusicFile(_ file : GTLRDrive_File ) -> Bool{
    if let ext = file.fileExtension {
        if Constants.MUSIC_FILE_EXT.contains(ext) {
            return true
        }
    }
    return false
}

func isMusicFile(_ file : OneDriveModel) -> Bool {
    let ext = file.name.getPathExtension()
    if Constants.MUSIC_FILE_EXT.contains(ext){
        return true
    }
    return false
}

func isMusicFile(_ file : Files.Metadata) -> Bool {
    if let ext = file.name.getFileExtension() {
        if Constants.MUSIC_FILE_EXT.contains(ext) {
            return true
        }
    }
    return false
}

func isVideoFile(_ file : GTLRDrive_File ) -> Bool{
    if let ext = file.fileExtension {
        if Constants.VIDEO_FILE_EXT.contains(ext) {
            return true
        }
    }
    return false
}

func isVideoFile(_ file : OneDriveModel) -> Bool {
    let ext = file.name.getPathExtension()
    if Constants.VIDEO_FILE_EXT.contains(ext){
        return true
    }
    return false
}

func isVideoFile(_ file : Files.Metadata) -> Bool {
    if let ext = file.name.getFileExtension() {
        if Constants.VIDEO_FILE_EXT.contains(ext) {
            return true
        }
    }
    return false
}

func isMusicURL(_ url : URL) -> Bool {
    if let ext = url.lastPathComponent.getFileExtension() {
        if Constants.MUSIC_FILE_EXT.contains(ext){
            return true
        }
    }
    return false
}

func isMusicFile(_ fileName : String) -> Bool {
    if let ext = fileName.getFileExtension() {
        if Constants.MUSIC_FILE_EXT.contains(ext){
            return true
        }
    }
    return false
}

func isVideoURL(_ url : URL) -> Bool {
    if let ext = url.lastPathComponent.getFileExtension() {
        if Constants.VIDEO_FILE_EXT.contains(ext){
            return true
        }
    }
    return false
}

func isVideoFile(_ fileName : String) -> Bool {
    if let ext = fileName.getFileExtension() {
        if Constants.VIDEO_FILE_EXT.contains(ext){
            return true
        }
    }
    return false
}

func createRootFolder() {
    if let documentDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first{
        let cardPath = documentDir.appendingPathComponent(Constants.ROOT)
        if !FileManager.default.fileExists(atPath: cardPath.path){
            do{
                try FileManager.default.createDirectory(at: cardPath, withIntermediateDirectories: false, attributes: nil)
                NSLog("CREATE \(Constants.ROOT) SUCCESS")
            }catch let error as NSError{
                print(error.localizedDescription)
            }
        }else{
            NSLog("DIRECTORY ROOT EXIST:  \(cardPath.path)")
        }
    }
}

func getAllFileByListPath(_ listPath : [String]) -> [MusicModel]{
    var listSong = [MusicModel]()
    for filename in listPath {
        if let realPath = getMusicWithURL(filename) {
            let playerItem = AVPlayerItem(url: URL(fileURLWithPath: realPath))
            let metadataList = playerItem.asset.commonMetadata
            var nameSong = "Unknowed"
            var artist = "Unknowed"
            var artwork : UIImage?
            for item in metadataList {
                if item.commonKey!.rawValue == "title" {
                    nameSong = item.stringValue!
                }
                if item.commonKey!.rawValue == "artist" {
                    artist = item.stringValue!
                }
                if item.commonKey!.rawValue == "artwork" {
                    if let data = item.dataValue {
                        artwork = UIImage(data: data)
                    }
                }
            }
            if nameSong == "Unknowed", let nameA = filename.getFileNameWithoutExt() {
                nameSong = nameA
            }
            let musicModel = MusicModel(nameSong, artist, artwork, path: filename)
            listSong.append(musicModel)
        } else {
        }
    }
    return listSong
}

func getAllVideoByListPath(_ listPath : [String]) -> [VideoModel]{
    var listVideo = [VideoModel]()
    for filename in listPath {
        if let realPath = getMusicWithURL(filename), isVideoFile(filename) {
            let playerItem = AVPlayerItem(url: URL(fileURLWithPath: realPath))
            let duration = playerItem.asset.duration
            let durationTime = CMTimeGetSeconds(duration)
            let metadataList = playerItem.asset.commonMetadata
            var nameVideo = "Unknowed"
            var artist = "Unknowed"
            var artwork : UIImage?
            for item in metadataList {
                if item.commonKey!.rawValue == "title" {
                    nameVideo = item.stringValue!
                }
                if item.commonKey!.rawValue == "artist" {
                    artist = item.stringValue!
                }
                
                if item.commonKey!.rawValue == "artwork" {
                    if let data = item.dataValue {
                        artwork = UIImage(data: data)
                    }
                }
            }
            if nameVideo == "Unknowed", let nameA = filename.getFileNameWithoutExt() {
                nameVideo = nameA
            }
            if artwork == nil {
                artwork = getThumbnailImage(forUrl: URL(fileURLWithPath: realPath))
            }
            let durationStr = getTimeString(seconds: Int(durationTime))
            let videoModel = VideoModel(nameVideo, artist, artwork, path: filename, duration: durationStr)
            videoModel.isPlay = false
            listVideo.append(videoModel)
        } else {
            print("Error : \(filename)")
        }
    }
    return listVideo
}

func getAllFileInFolder() -> [MusicModel]{
    do {
        let fileList = try FileManager.default.contentsOfDirectory(atPath: getDirRootMusicPath())
        var listAllSong = [MusicModel]()
        for filename in fileList {
            if let realPath = getMusicWithURL(filename), isMusicFile(filename) {
                let playerItem = AVPlayerItem(url: URL(fileURLWithPath: realPath))
                let metadataList = playerItem.asset.commonMetadata
                var nameSong = "Unknowed"
                var artist = "Unknowed"
                var artwork : UIImage?
                for item in metadataList {
                    if item.commonKey!.rawValue == "title" {
                        nameSong = item.stringValue!
                    }
                    if item.commonKey!.rawValue == "artist" {
                        artist = item.stringValue!
                    }
                    
                    if item.commonKey!.rawValue == "artwork" {
                        if let data = item.dataValue {
                            artwork = UIImage(data: data)
                        }
                    }
                }
                if nameSong == "Unknowed", let nameA = filename.getFileNameWithoutExt() {
                    nameSong = nameA
                }
                let musicModel = MusicModel(nameSong, artist, artwork, path: filename)
                listAllSong.append(musicModel)
            } else {
                print("Error : \(filename)")
            }
        }
        return listAllSong
    } catch let err {
        print(err.localizedDescription)
        return []
    }
}

func getAllVideoInFolder() -> [VideoModel] {
    do {
        let fileList = try FileManager.default.contentsOfDirectory(atPath: getDirRootMusicPath())
        var listAllVideo = [VideoModel]()
        for filename in fileList {
            if let realPath = getMusicWithURL(filename), isVideoFile(filename) {
                let playerItem = AVPlayerItem(url: URL(fileURLWithPath: realPath))
                let duration = playerItem.asset.duration
                let durationTime = CMTimeGetSeconds(duration)
                let metadataList = playerItem.asset.commonMetadata
                var nameVideo = "Unknowed"
                var artist = "Unknowed"
                var artwork : UIImage?
                for item in metadataList {
                    if item.commonKey!.rawValue == "title" {
                        nameVideo = item.stringValue!
                    }
                    if item.commonKey!.rawValue == "artist" {
                        artist = item.stringValue!
                    }
                    
                    if item.commonKey!.rawValue == "artwork" {
                        if let data = item.dataValue {
                            artwork = UIImage(data: data)
                        }
                    }
                }
                if nameVideo == "Unknowed", let nameA = filename.getFileNameWithoutExt() {
                    nameVideo = nameA
                }
                if artwork == nil {
                    artwork = getThumbnailImage(forUrl: URL(fileURLWithPath: realPath))
                }
                let durationStr = getTimeString(seconds: Int(durationTime))
                let videoModel = VideoModel(nameVideo, artist, artwork, path: filename, duration: durationStr)
                videoModel.isPlay = false
                listAllVideo.append(videoModel)
            } else {
                print("Error : \(filename)")
            }
        }
        return listAllVideo
    } catch let err {
        print(err.localizedDescription)
        return []
    }
}

func renameAlbum(_ index : Int, _ name : String){
    let listAlbum = getListAlbum()
    if index <= listAlbum.count - 1 {
        listAlbum[index].name = name
        let userDefaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: listAlbum)
        userDefaults.set(encodedData, forKey: Constants.LIST_ALBUM)
        userDefaults.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
    }
}

func removeAlbum(_ index : Int){
    var listAlbum = getListAlbum()
    listAlbum.remove(at: index)
    let userDefaults = UserDefaults.standard
    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: listAlbum)
    userDefaults.set(encodedData, forKey: Constants.LIST_ALBUM)
    userDefaults.synchronize()
    NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
}

func updateListSongInAlbum(_ listPath : [String]) -> [String]{
    if listPath.isEmpty {
        return []
    }
    var listNewsPath = [String]()
    for item in listPath {
        if getMusicWithURL(item) != nil {
            listNewsPath.append(item)
        }
    }
    return listNewsPath
}

func getMusicWithURL(_ musicName : String) -> String? {
    let realPath = getDirRootMusicPath() + "/\(musicName)"
    if FileManager.default.fileExists(atPath: realPath){
        return realPath
    }
    return nil
}

func isSongExists(musicName : String) -> Bool {
    let pathCheck = getDirRootMusicPath() + "/\(musicName)"
    if FileManager.default.fileExists(atPath: pathCheck){
        return true
    }
    return false
}

func playAlbumFromDetail(lstSong : [MusicModel], indexPlaying: Int, isRandom : Bool) {
    if let delegate = UIApplication.shared.delegate as? AppDelegate {
        if let viewPlay = delegate.viewPlay {
            viewPlay.playAlbum(lstSong: lstSong,indexPlay: indexPlaying,isRandom: isRandom)
        } else {
            delegate.addViewPlay()
            if let viewPlay = delegate.viewPlay {
                viewPlay.playAlbum(lstSong: lstSong,indexPlay: indexPlaying,isRandom: isRandom)
            }
        }
    }
}

func resetBage() {
    if let delegate = UIApplication.shared.delegate as? AppDelegate {
        if let tabbar = delegate.tabbar {
            guard !tabbar.viewControllers.isEmpty else {
                return
            }
            if let customTab = tabbar.customTab {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2 ) {
                    UserDefaults.standard.set(0, forKey: Constants.NUMBER_NOTIFY)
                    if customTab.hubCart != nil {
                        customTab.hubCart.setCount(0)
                    }
                }
            }
        }
    }
}

func updateBadge() {
    if let delegate = UIApplication.shared.delegate as? AppDelegate {
        if let tabbar = delegate.tabbar {
            guard !tabbar.viewControllers.isEmpty else {
                return
            }
            if let customTab = tabbar.customTab {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2 ) {
                    var numberNoty = UserDefaults.standard.integer(forKey: Constants.NUMBER_NOTIFY)
                    numberNoty += 1
                    UserDefaults.standard.set(numberNoty, forKey: Constants.NUMBER_NOTIFY)
                    if numberNoty > 0 {
                        if customTab.hubCart != nil {
                            customTab.hubCart.setCount(numberNoty)
                        }
                    }
                }
            }
        }
    }
}

func deleteFileByPath(_ filePath : String, _ isOne : Bool = true) {
    
    do {
        let fileManager = FileManager.default
        
        // Check if file exists
        if fileManager.fileExists(atPath: getMusicWithURL(filePath)!) {
            // Delete file
            try fileManager.removeItem(atPath: getMusicWithURL(filePath)!)
            if isOne {
                NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
            }
        } else {
            print("File does not exist")
        }
        
    } catch let error as NSError {
        print("An error took place: \(error)")
    }
}

func removeListFile(_ listPath : [String]){
    if listPath.isEmpty {
        return
    }
    for item in listPath {
        deleteFileByPath(item, false)
    }
    NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
}

func addToAlbum(_ index : Int, _ path : String) {
    let listAlbum = getListAlbum()
    listAlbum[index].listSong.append(path)
    let userDefaults = UserDefaults.standard
    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: listAlbum)
    userDefaults.set(encodedData, forKey: Constants.LIST_ALBUM)
    userDefaults.synchronize()
    NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
}

func deleteSongInAlbum(_ indexAlbum : Int, _ indexSong : Int ){
    let listAlbum = getListAlbum()
    listAlbum[indexAlbum].listSong.remove(at: indexSong)
    let userDefaults = UserDefaults.standard
    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: listAlbum)
    userDefaults.set(encodedData, forKey: Constants.LIST_ALBUM)
    userDefaults.synchronize()
    NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
}

func addListSongToAlbum(_ index : Int, _ listPath : [String]){
    let listAlbum = getListAlbum()
    listAlbum[index].listSong.append(contentsOf: listPath)
    let userDefaults = UserDefaults.standard
    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: listAlbum)
    userDefaults.set(encodedData, forKey: Constants.LIST_ALBUM)
    userDefaults.synchronize()
    NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
}

func addListVideoToAlbum(_ index : Int, _ listPath : [String]){
    let listAlbum = getListAlbum()
    listAlbum[index].listSong.append(contentsOf: listPath)
    let userDefaults = UserDefaults.standard
    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: listAlbum)
    userDefaults.set(encodedData, forKey: Constants.LIST_ALBUM)
    userDefaults.synchronize()
    NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
}


func getListAlbum() -> [AlbumModel]{
    guard let decoded  = UserDefaults.standard.data(forKey: Constants.LIST_ALBUM) else {
        return []
    }
    let decodedAlbums = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [AlbumModel]
    var listAlbum = [AlbumModel]()
    for item in decodedAlbums {
        let list = updateListSongInAlbum(item.listSong)
        let name = item.name
        let album = AlbumModel(name: name, listSong: list, isVideo: item.isVideo)
        listAlbum.append(album)
    }
    let userDefaults = UserDefaults.standard
    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: listAlbum)
    userDefaults.set(encodedData, forKey: Constants.LIST_ALBUM)
    userDefaults.synchronize()
    return listAlbum
}

func createNewsAlbum(_ newAlbum : AlbumModel){
    var listAlbum = getListAlbum()
    listAlbum.append(newAlbum)
    let userDefaults = UserDefaults.standard
    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: listAlbum)
    userDefaults.set(encodedData, forKey: Constants.LIST_ALBUM)
    userDefaults.synchronize()
    NotificationCenter.default.post(name: NSNotification.Name(Constants.UPDATE_LIST_ALBUM), object: nil, userInfo: nil)
}

func getDirRootMusicPath() -> String{
    let documentDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    let cardDir = documentDir!.appendingPathComponent(Constants.ROOT)
    return cardDir.path
}

func openURL(destURL : String){
    if let url = URL(string: destURL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            if UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
}

func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
    return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
}

func getTimeString(seconds : Int) -> String {
    let currentTime = seconds
    let (_,m,s) = secondsToHoursMinutesSeconds(seconds: currentTime)
    let durationStr = String(format:"%02d:%02d", m, s)
    
    return durationStr
}

extension String {
    func getPathExtension() -> String {
        return (self as NSString).pathExtension
    }
    
    func getName() -> String {
        return (self as NSString).deletingPathExtension
    }
}

func isCanDownloadVideo(bytes: Int64) -> Bool {
    if bytes < 1024 {
        return true
    } else {
        let size = Double(bytes/1024/1024)
        if size > Constants.MAX_VIDEO_SIZE {
            return false
        }
    }
    return true
}

func getThumbnailImage(forUrl url: URL) -> UIImage? {
    let asset: AVAsset = AVAsset(url: url)
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    
    do {
        let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
        return UIImage(cgImage: thumbnailImage)
    } catch let error {
        print(error)
    }
    
    return nil
}

func addWaveBackground(to view: UIView){
    let leftDrop:CGFloat = 0.4
    let rightDrop: CGFloat = 0.3
    let leftInflexionX: CGFloat = 0.4
    let leftInflexionY: CGFloat = 0.47
    let rightInflexionX: CGFloat = 0.6
    let rightInflexionY: CGFloat = 0.22
    
    let backView = UIView(frame: view.frame)
    backView.backgroundColor = .clear
    view.addSubview(backView)
    let backLayer = CAShapeLayer()
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 0, y: 50))
    path.addLine(to: CGPoint(x:0, y: view.frame.height * leftDrop))
    path.addCurve(to: CGPoint(x:view.frame.width, y: view.frame.height * rightDrop),
                  controlPoint1: CGPoint(x: view.frame.width * leftInflexionX, y: view.frame.height * leftInflexionY),
                  controlPoint2: CGPoint(x: view.frame.width * rightInflexionX, y: view.frame.height * rightInflexionY))
    path.addLine(to: CGPoint(x:view.frame.width, y: 0))
    path.close()
    backLayer.fillColor = UIColor.blue.cgColor
    backLayer.path = path.cgPath
    backView.layer.addSublayer(backLayer)
}
